{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="quickorder-wizard-module-confirmation">
	<h2 class="quickorder-wizard-module-confirmation-title">{{translate 'Thank you!'}}</h2>
	<p class="quickorder-wizard-module-confirmation-body">
		{{translate 'Your Items have been successfully added to the cart.'}}
	</p>
	<!-- <p class="quickorder-wizard-module-confirmation-body">
		{{{confirmationMessage}}}
	</p> -->
	<!-- <p class="quickorder-wizard-module-confirmation-body">
		{{#if hasSalesrep}}
			{{translate 'For immediate assistance call us at <strong>$(0)</strong> or email us at <a href="mailto:$(1)">$(1)</a>' salesrepPhone salesrepEmail}}
		{{else}}
			{{{disclaimer}}}
		{{/if}}
	</p> -->
	<a class="quickorder-wizard-module-confirmation-new-quote" href="/quick-order">{{translate 'Add More Items'}}</a>
	<a class="quickorder-wizard-module-confirmation-continue" href="#" data-touchpoint="viewcart" data-hashtag="#cart"  data-action="view-cart">{{translate 'View Cart'}}</a>
</div>
