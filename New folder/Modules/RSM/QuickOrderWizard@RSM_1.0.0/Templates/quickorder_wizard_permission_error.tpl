{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="quickorder-wizard-permission-error">
	<div class="quickorder-wizard-permission-error-header">
		<h1 class="quickorder-wizard-permission-error-header-title">
			{{pageHeader}}
		</h1>
	</div>
	<div class="quickorder-wizard-permission-error-message">
		 <p class="quickorder-wizard-permission-error-message-disclaimer">{{translate 'Sorry, you don\'t have sufficient permissions to request a quote online. <br/> For immediate assistance <strong>call us at $(0)</strong> or email us to <strong>$(1)</strong>' salesrepPhone salesrepEmail}}</p>
		<a href="/" data-touchpoint="home" class="quickorder-wizard-permission-error-button">{{translate 'Go to Home Page'}}</a>
	</div>
</div>
