{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div>
	<header>
		<h1 class="quickorder-wizard-step-header-title">{{currentStepGroupName}}</h1>
	</header>
</div>

<div data-type="alert-placeholder-step"></div>

<div class="quickorder-wizard-step-content-wrapper">

	<div id="wizard-step-content" class="quickorder-wizard-step-content-main"></div>

	{{#if showNavButtons}}
		<div class="quickorder-wizard-step-actions">
			<div class="quickorder-wizard-step-button-container">
				{{#if showContinueButton}}
					<button class="quickorder-wizard-step-button-continue" data-action="submit-step">
						{{continueButtonLabel}}
					</button>
				{{/if}}
				{{#if showBackButton}}
					<button class="quickorder-wizard-step-button-back" data-action="previous-step">
						{{translate 'Back'}}
					</button>
				{{/if}}
			</div>
		</div>
	{{/if}}

	{{#if showBottomMessage}}
		<div class="quickorder-wizard-step-content-wrapper-bottom-content">
			<p class="quickorder-wizard-step-content-wrapper-disclaimer-message">
				{{{bottomMessage}}}
			</p>
		</div>
	{{/if}}
</div>
