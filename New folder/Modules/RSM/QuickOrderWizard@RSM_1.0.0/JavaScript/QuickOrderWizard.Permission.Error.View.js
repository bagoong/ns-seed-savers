/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrderWizard
define('QuickOrderWizard.Permission.Error.View'
,	[
		'SC.Configuration'

	,	'quickorder_wizard_permission_error.tpl'

	,	'Backbone'
	,	'underscore'
	]
,	function (
		Configuration

	,	quickorder_wizard_permission_error_tpl

	,	Backbone
	,	_
	)
{
	'use strict';

	//@class QuickOrderWizard.Permission.Error.View @extend Backbone.View
	return Backbone.View.extend({

		// @property {Function} template
		template: quickorder_wizard_permission_error_tpl

		// @property {String} page_header
	,	page_header: _('Quick Order').translate()

		// @property {String} title
	,	title: _('Quick Order').translate()

		//@property {String} bodyClass This property indicate the class used on the body to remove the My Account side menu
	,	bodyClass: 'force-hide-side-nav'

		//@method getContext
		//@return {QuickOrderWizard.Permission.Error.View.Context}
	,	getContext: function ()
		{
			//@class QuickOrderWizard.Permission.Error.View.Context
			return {
				// @property {String} pageHeader
				pageHeader: this.page_header
				// @property {Boolean} showSalesRepInformation Phone
			,	salesrepPhone: Configuration.get('quote.defaultPhone')
				// @property {Boolean} showSalesRepInformation Email
			,	salesrepEmail: Configuration.get('quote.defaultEmail')
			};
			//@class QuickOrderWizard.Permission.Error.View
		}

	});
});
