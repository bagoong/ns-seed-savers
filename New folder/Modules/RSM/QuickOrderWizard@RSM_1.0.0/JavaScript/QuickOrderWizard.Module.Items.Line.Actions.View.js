/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module QuickOrderWizard
define('QuickOrderWizard.Module.Items.Line.Actions.View'
,	[
		'quickorder_wizard_module_items_line_actions.tpl'

	,	'Backbone'
	]
,	function (
		quickorder_wizard_module_items_line_actions_tpl

	,	Backbone
	)
{
	'use strict';

	//@class QuickOrderWizard.Module.Items.Line.Actions.View @extend Backbone.View
	return Backbone.View.extend({

		//@property {Function} template
		template: quickorder_wizard_module_items_line_actions_tpl

		//@property {Object} events
	,	events: {
			'click [data-action="remove"]': 'removeItem'
		}

		//@method removeItem Removed the current item from the list of items
		//@return {Void}
	,	removeItem: function ()
		{
			this.options.model.collection.remove(this.options.model);
		}

		//@method getContext
		//@return {QuickOrderWizard.Module.Items.Line.Actions.View.Context}
	,	getContext: function ()
		{
			//@class QuickOrderWizard.Module.Items.Line.Actions.View.Context
			return {
			};
			//@class QuickOrderWizard.Module.Items.Line.Actions.View
		}
	});

});
