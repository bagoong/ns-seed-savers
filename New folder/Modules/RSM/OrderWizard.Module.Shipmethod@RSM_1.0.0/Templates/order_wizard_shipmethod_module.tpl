{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="order-wizard-shipmethod-module">
	{{#if showTitle}}
		<h3 class="order-wizard-shipmethod-module-title">
			{{title}}
		</h3>
	{{/if}}

	{{#if showEnterShippingAddressFirst}}
		<div class="order-wizard-shipmethod-module-message">
			{{translate 'Warning: Please enter a valid shipping address first'}}
		</div>
	{{else}}
		{{#if showLoadingMethods}}
			<div class="order-wizard-shipmethod-module-message">
				{{translate 'Loading...'}}
			</div>
		{{else}}

		{{#if containsServiceOnly}}
		<p>
			{{translate '*No Shipping.'}}
		<p>
		{{else}}
			{{#if hasPsmShipGroups}}
				{{#if displayPacejetError}}
					<h2>{{pacejetError}}</h2>
				{{else}}

				<p>{{translate 'To receive orders by Christmas:'}}</p><br>
				<ul class="shipping-list">
					<li>{{translate 'You will need to select the appropriate expedited shipping (fast or faster) option.'}}</li>
					<li>{{translate 'Expedited orders received by 11:59am CDT on weekdays will go out the same day, if placed after 11:59am CST; next business day.'}}</li>
					<li>{{translate 'No Saturday delivery.'}}</li>
					<li>{{translate 'Mailed Gift Cards: Order by Dec 19th (end of day)'}}</li>
					<li>{{translate 'eGift Cards:  Order by 11:59am CDT Dec 22nd'}}</li>
				</ul><br>
				<!-- <p>{{translate 'Regular Orders: Order by noon (CDT) Dec. 14 | Mailed Gift Cards: Order by Dec. 19 (end of day) | eGift Cards: Order by noon (CDT) Dec. 22nd!'}}</p><br> -->
				<p> {{translate '-For all items shipping later, you will receive an email notice one week prior to shipment.'}} </p>
				<p> {{translate '-Fast: Two-Day Shipping'}} </p>
				<p> {{translate '-Faster: Overnight Shipping'}} </p>
					<br>
				{{#each psmShipGroups}}
					<h2>{{psmName}} - {{seasonDisplay}}</h2>
					{{#if standardShipping}}
					<a data-action="select-psm-delivery-option-radio"
					class="order-wizard-shipmethod-module-option {{#if standardShipping.isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
					data-value="{{standardShipping.shipMethodId}}">
						<input type="radio" name="delivery-options-{{psmCode}}" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox"
						{{#if standardShipping.isActive}}checked{{/if}}
						value="{{standardShipping.shipMethodId}}"
						id="{{psmCode}}-{{standardShipping.shipMethodId}}-{{standardShipping.shippingSpeed}}" />

						<span class="order-wizard-shipmethod-module-option-name">{{standardShipping.shippingSpeed}}
							<span class="order-wizard-shipmethod-module-option-price">{{standardShipping.rateAndHandling_formatted}}</span>
						</span>
					</a>
					{{/if}}
					{{#if fastShipping}}
					<a data-action="select-psm-delivery-option-radio"
					class="order-wizard-shipmethod-module-option {{#if fastShipping.isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
					data-value="{{fastShipping.shipMethodId}}">
						<input type="radio" name="delivery-options-{{psmCode}}" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox"
						{{#if fastShipping.isActive}}checked{{/if}}
						value="{{fastShipping.shipMethodId}}"
						id="{{psmCode}}-{{fastShipping.shipMethodId}}-{{fastShipping.shippingSpeed}}" />

						<span class="order-wizard-shipmethod-module-option-name">{{fastShipping.shippingSpeed}}
							<span class="order-wizard-shipmethod-module-option-price">{{fastShipping.rateAndHandling_formatted}}</span>
						</span>
					</a>
					{{/if}}
					{{#if fasterShipping}}
					<a data-action="select-psm-delivery-option-radio"
					class="order-wizard-shipmethod-module-option {{#if fasterShipping.isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
					data-value="{{fasterShipping.shipMethodId}}">
						<input type="radio" name="delivery-options-{{psmCode}}" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox"
						{{#if fasterShipping.isActive}}checked{{/if}}
						value="{{fasterShipping.shipMethodId}}"
						id="{{psmCode}}-{{fasterShipping.shipMethodId}}-{{fasterShipping.shippingSpeed}}" />

						<span class="order-wizard-shipmethod-module-option-name">{{fasterShipping.shippingSpeed}}
							<span class="order-wizard-shipmethod-module-option-price">{{fasterShipping.rateAndHandling_formatted}}</span>
						</span>
					</a>
					{{/if}}
				{{/each}}
				{{/if}}
			<!-- {{#if displayPacejetError}} -->
			<!-- {{else}} -->
			<!-- {{#if hasShippingMethods}}
				{{#if showSelectForShippingMethod}}
					<select data-action="select-delivery-option" data-action="edit-module" class="order-wizard-shipmethod-module-option-select">
						<option>{{translate 'Select a delivery method'}}</option>
						{{#each shippingMethods}}
							<option
							{{#if isActive}}selected{{/if}}
							value="{{internalid}}"
							id="delivery-options-{{internalid}}">
								{{rate_formatted}} - {{name}}
							</option>
						{{/each}}
					</select>
				{{else}}
					<h2>Other</h2>
					{{#each shippingMethods}}
						<a data-action="select-delivery-option-radio"
						class="order-wizard-shipmethod-module-option {{#if isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
						data-value="{{internalid}}">
							<input type="radio" name="delivery-options" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox"
							{{#if isActive}}checked{{/if}}
							value="{{internalid}}"
							id="delivery-options-{{internalid}}" />

							<span class="order-wizard-shipmethod-module-option-name">{{name}}
								<span class="order-wizard-shipmethod-module-option-price">{{rate_formatted}}</span>
							</span>
						</a>
					{{/each}}
				{{/if}} -->
			{{else}}
				<div class="order-wizard-shipmethod-module-message">
					{{translate 'Warning: No Delivery Methods are available for this address'}}
				</div>
			{{/if}}
			{{/if}}
			<!-- {{/if}} -->
		{{/if}}
	{{/if}}
</div>
