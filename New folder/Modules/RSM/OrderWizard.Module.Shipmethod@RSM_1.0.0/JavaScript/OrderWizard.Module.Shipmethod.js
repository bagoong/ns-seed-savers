/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderWizard.Module.Shipmethod
define(
	'OrderWizard.Module.Shipmethod'
,	[	'Wizard.Module'
	,	'Profile.Model'
	,	'GlobalViews.Message.View'

	,	'order_wizard_shipmethod_module.tpl'

	,	'underscore'
	,	'jQuery'
	,	'blockui'
	]
,	function (
		WizardModule
	,	ProfileModel
	,	GlobalViewsMessageView

	,	order_wizard_shipmethod_module_tpl

	,	_
	,	jQuery
	)
{
	'use strict';
	//@class OrderWizard.Module.Shipmethod @extends Wizard.Module
	return WizardModule.extend({

		//@property {Function} template
		template: order_wizard_shipmethod_module_tpl
		//@property {Object} events
	,	events: {
			'change [data-action="select-delivery-option"]': 'changeDeliveryOptions'
		,	'click [data-action="select-delivery-option-radio"]': 'changeDeliveryOptions'
		,	'click [data-action="select-psm-delivery-option-radio"]': 'changePsmDeliveryOptions'
		}
		//@property {Array} errors
	,	errors: ['ERR_CHK_SELECT_SHIPPING_METHOD','ERR_WS_INVALID_SHIPPING_METHOD']
		//@method initialize
	,	initialize: function ()
		{
			this.waitShipmethod = SC.ENVIRONMENT.CART ? !SC.ENVIRONMENT.CART.shipmethod : !(this.model && this.model.get('shipmethod'));

			this.profileModel = ProfileModel.getInstance();

			this.addresses = this.profileModel.get('addresses');

			WizardModule.prototype.initialize.apply(this, arguments);
			// So we allways have a the reload promise
			this.reloadMethodsPromise = jQuery.Deferred().resolve();
			this.wizard.model.on('ismultishiptoUpdated', _.bind(this.render, this));
		}

	,	isActive: function ()
		{
			var is_shipping_required = this.wizard.model.shippingAddressIsRequired();
			if (!is_shipping_required)
			{
				this.wizard.model.unset('shipmethod');
			}
			return is_shipping_required && !this.wizard.model.get('ismultishipto');
		}

		//@method present
	,	present: function ()
		{
			this.currentAddress = this.previousAddress = this.model.get('shipaddress');
			this.eventHandlersOn();
		}
		//@method future
	,	future: function()
		{
			this.currentAddress = this.previousAddress = this.model.get('shipaddress');
			this.eventHandlersOn();
		}
		//@method past
	,	past: function()
		{
			this.waitShipmethod = !this.model.get('shipmethod');
			this.currentAddress = this.previousAddress = this.model.get('shipaddress');
			this.eventHandlersOn();
		}
		//@method eventHandlersOn
	,	eventHandlersOn: function ()
		{
			// Removes any leftover observer
			this.eventHandlersOff();
			// Adds the observer for this step
			this.model.on('change:shipaddress', this.shipAddressChange, this);

			this.model.on('change:shipmethods', function ()
			{
				_.defer(_.bind(this.render, this));
			}, this);

			var selected_address = this.addresses.get(this.currentAddress);

			if (selected_address)
			{
				selected_address.on('sync', jQuery.proxy(this, 'reloadMethods'), this);
			}
		}
		//@method eventHandlersOff
	,	eventHandlersOff: function ()
		{
			// removes observers
			this.model.off('change:shipmethods', null, this);
			this.model.off('change:shipaddress', this.shipAddressChange, this);

			var current_address = this.addresses.get(this.currentAddress)
			,	previous_address = this.addresses.get(this.previousAddress);

			if (current_address)
			{
				current_address.off('change:country change:zip', null, this);
				current_address.off('sync');
			}

			if (previous_address && previous_address !== current_address)
			{
				previous_address.off('change:country change:zip', null, this);
			}
		}
		//@method render
	,	render: function ()
		{
			if (this.state === 'present')
			{
				if (this.model.get('shipmethod') && !this.waitShipmethod)
				{
					this.trigger('ready', true);
				}
				this._render();
			}
		}
		//@method shipAddressChange
	,	shipAddressChange: function (model, value)
		{
			// if its not null and there is a difference we reload the methods
			if (this.currentAddress !== value)
			{
				this.currentAddress = value;

				var order_address = this.model.get('addresses')
				,	previous_address = this.previousAddress && (order_address.get(this.previousAddress) || this.addresses.get(this.previousAddress))
				,	current_address = this.currentAddress && order_address.get(this.currentAddress) || this.addresses.get(this.currentAddress)
				,	changed_zip = previous_address && current_address && previous_address.get('zip') !== current_address.get('zip')
				,	changed_state = previous_address && current_address && previous_address.get('state') !== current_address.get('state')
				,	changed_country = previous_address && current_address && previous_address.get('country') !== current_address.get('country');

				// if previous address is equal to current address we compare the previous values on the model.
				if (this.previousAddress && this.currentAddress && this.previousAddress === this.currentAddress)
				{
					changed_zip = current_address.previous('zip') !== current_address.get('zip');
					changed_country = current_address.previous('country') !== current_address.get('country');
					changed_state = current_address.previous('state') !== current_address.get('state');
				}

				// reload ship methods only if there is no previous address or when change the country or zipcode
				if ((!previous_address && current_address) || changed_zip || changed_country || changed_state)
				{
					// if its selected a valid address, reload Methods
					if (this.model.get('isEstimating') || this.addresses.get(this.model.get('shipaddress')))
					{
						this.reloadMethods();
					}
				}
				else
				{
					this.render();
				}

				if (value)
				{
					this.previousAddress = value;
				}

				// if we select a new address, bind the sync method for possible address edits
				if (this.currentAddress)
				{
					var selected_address = this.addresses.get(this.currentAddress);
					if(selected_address)
					{
						selected_address.on('sync', jQuery.proxy(this, 'reloadMethods'), this);
					}

					// if there was a different previous address, remove the sync handler
					if(this.previousAddress && this.previousAddress !== this.currentAddress)
					{
						var previous_selected_address = this.addresses.get(this.previousAddress);
						if(previous_selected_address)
						{
							previous_selected_address.off('sync');
						}
					}
				}
			}
		}
		//@method reloadMethods
	,	reloadMethods: function ()
		{
			if (this.model.get('confirmation').internalid ||  this.model.get('confirmation').id )
			{
				return;
			}
			// to reload the shipping methods we just save the order
			var self = this
			,	$container = this.$el;

			//doesn't save the current ship methods because of an address change
			this.model.set('resetPsmShipMethods', true);

			$container.addClass('loading');

			// Abort the previous ajax call
			this.reloadMethodsPromise.abort && this.reloadMethodsPromise.abort();
			this.reloadingMethods = true;
			this.render();
			this.reloadMethodsPromise = this.model.save(null, {
				parse: false
			,	success: function (model, attributes)
				{
					model.set({
							shipmethods: attributes.shipmethods
						,	shipmethod: attributes.shipmethod
						,	summary: attributes.summary
						,	psmShipGroups: attributes.psmShipGroups
						,	psmShipMethods: attributes.psmShipMethods
						,	resetPsmShipMethods: attributes.resetPsmShipMethods
						,	containsServiceOnly: attributes.containsServiceOnly
					});
				}
			}).always(function (xhr)
			{
				// .always() method is excecuted even if the ajax call was aborted
				if (xhr.statusText !== 'abort')
				{
					$container.removeClass('loading');
					self.render();
					self.step.enableNavButtons();
					self.reloadingMethods = false;
				}
			});

			if (this.reloadMethodsPromise.state() === 'pending')
			{
				self.step.disableNavButtons();
			}
		}
		//@method submit
	,	submit: function ()
		{
			return this.isValid();
		}
		//@method isValid
	,	isValid: function ()
		{
			var model = this.model
			,	valid_promise = jQuery.Deferred();

			var containsServiceOnly = model.get('containsServiceOnly')
			if (!containsServiceOnly) {
				var rejectPsmShipMethodObj = this.checkPsmShipMethods(model);
			}


			this.reloadMethodsPromise.always(function ()
			{
				if (!containsServiceOnly && rejectPsmShipMethodObj.reject) {
					if (rejectPsmShipMethodObj.psmName == '') {
						valid_promise.reject({
							errorCode: 'ERR_CHK_SELECT_SHIPPING_METHOD'
						,	errorMessage: _('Please select a delivery method').translate()
						});
					}
					else {
						valid_promise.reject({
							errorCode: 'ERR_CHK_SELECT_SHIPPING_METHOD'
						,	errorMessage: _('Please select a delivery method for ' + rejectPsmShipMethodObj.psmName).translate()
						});
					}
				}
				else if (model.get('shipmethod') && model.get('shipmethods').get(model.get('shipmethod')))
				{
					valid_promise.resolve();
				}
				else
				{
					valid_promise.reject({
						errorCode: 'ERR_CHK_SELECT_SHIPPING_METHOD'
					,	errorMessage: _('Please select a delivery method').translate()
					});
				}
			});

			return valid_promise;
		}

	,	checkPsmShipMethods: function(order)
		{
			var rejectObj = {
				reject: false
			,	psmName: ''
			}
			var psmShipGroups = order.get('psmShipGroups');
			var psmShipMethods = order.get('psmShipMethods');

			if (psmShipMethods == null) {
				rejectObj.reject = true;
				return rejectObj;
			}
			else {
				var found;
				var psmCode;
				var psmName;
				for (var i = 0; i < psmShipGroups.length; i++) {
					psmCode = psmShipGroups[i].psmCode;
					psmName = psmShipGroups[i].psmName;
					found = false;
					for (var x = 0; x < psmShipMethods.length; x++) {
						if (psmShipMethods[x].psmCode == psmCode) {
							found = true;
							break;
						}
					}

					if (!found) {
						rejectObj.reject = true;
						rejectObj.psmName = psmName;
						break;
					}
				}

				return rejectObj;
			}
		}

		//@method changeDeliveryOptions
	,	changeDeliveryOptions: function (e)
		{
			var self = this
			,	target = jQuery(e.currentTarget)
			,	targetValue = target.val() || target.attr('data-value');

			this.waitShipmethod = true;
			this.model.set('shipmethod', targetValue);
			this.step.disableNavButtons();
			this.model.save().always(function()
			{
				self.clearError();
				self.step.enableNavButtons();
			});
		}

	,	changePsmDeliveryOptions: function (e)
		{
			var self = this
			,	target = jQuery(e.currentTarget)
			,	targetValue = target.val() || target.attr('data-value');

			this.step.disableNavButtons();
			jQuery.blockUI({
				message: '<h1>Saving Delivery Method...</h1>',
				css: {
					width: '100%',
					left: ''
				}
			});

			var psmCodeAndShipmethodAndShippingSpeed = target.find('input[type=radio]').attr('id').split('-');
			var psmCode = psmCodeAndShipmethodAndShippingSpeed[0];
			var shipMethodId = psmCodeAndShipmethodAndShippingSpeed[1];
			var shippingSpeed = psmCodeAndShipmethodAndShippingSpeed[2];

			var pacejetRate;
			var handling;
			var rateAndHandling;
			var psmName;
			var weight;
			var shipMethodName;
			var carrier;
			var startDate;

			var psmShipGroups = this.model.get('psmShipGroups');

			for (var i = 0; i < psmShipGroups.length; i++) {
				if (psmShipGroups[i].psmCode == psmCode) {
					psmName = psmShipGroups[i].psmName;
					weight = psmShipGroups[i].weight;
					startDate = psmShipGroups[i].startDate;
					if (shippingSpeed == 'Standard') {
						pacejetRate = psmShipGroups[i].standardShipping.pacejetRate;
						handling = psmShipGroups[i].standardShipping.handling;
						rateAndHandling = psmShipGroups[i].standardShipping.rateAndHandling;
						shipMethodName = psmShipGroups[i].standardShipping.shipMethodName;
					}
					else if (shippingSpeed == 'Fast') {
						pacejetRate = psmShipGroups[i].fastShipping.pacejetRate;
						handling = psmShipGroups[i].fastShipping.handling;
						rateAndHandling = psmShipGroups[i].fastShipping.rateAndHandling;
						shipMethodName = psmShipGroups[i].standardShipping.shipMethodName;
					}
					else if (shippingSpeed == 'Faster') {
						pacejetRate = psmShipGroups[i].fasterShipping.pacejetRate;
						handling = psmShipGroups[i].fasterShipping.handling;
						rateAndHandling = psmShipGroups[i].fasterShipping.rateAndHandling;
						shipMethodName = psmShipGroups[i].standardShipping.shipMethodName;
					}

					break;
				}
			}

			if (shipMethodName.indexOf('UPS') > -1){
				carrier = 'ups';
			}
			else {
				carrier = 'nonups';
			}

			var psmShipMethods = this.model.get('psmShipMethods');

			//if no psm ship Methods found then create one and add to model
			//if there are loop through, if the psm code is found update the ship method related it, else add the new psm code and the related ship method
			if (psmShipMethods != null) {
				var hasProp = false;
				var propIndex;
				for (var x = 0; x < psmShipMethods.length; x++) {
					if (psmShipMethods[x].psmCode == psmCode) {
						hasProp = true;
						propIndex = x;
						break;
					}
				}
				if (hasProp) {
					psmShipMethods[propIndex].psmName = psmName;
					psmShipMethods[propIndex].shipMethodId = shipMethodId;
					psmShipMethods[propIndex].pacejetRate = pacejetRate;
					psmShipMethods[propIndex].handling = handling;
					psmShipMethods[propIndex].rateAndHandling = rateAndHandling;
					psmShipMethods[propIndex].shippingSpeed = shippingSpeed;
					psmShipMethods[propIndex].weight = weight;
					psmShipMethods[propIndex].carrier = carrier;
					psmShipMethods[propIndex].startDate = startDate;
				}
				else {
					psmShipMethods.push({
						psmCode: psmCode
					,	psmName: psmName
					,	shipMethodId: shipMethodId
					,	pacejetRate: pacejetRate
					,	handling: handling
					,	rateAndHandling: rateAndHandling
					,	shippingSpeed: shippingSpeed
					,	weight: weight
					,	carrier: carrier
					,	startDate: startDate
					});
				}
				this.model.set('psmShipMethods', psmShipMethods);
			}

			else {
				psmShipMethods = [];
				psmShipMethods.push({
					psmCode: psmCode
				,	psmName: psmName
				,	shipMethodId: shipMethodId
				,	pacejetRate: pacejetRate
				,	handling: handling
				,	rateAndHandling: rateAndHandling
				,	shippingSpeed: shippingSpeed
				,	weight: weight
				,	carrier: carrier
				,	startDate: startDate
				});
				this.model.set('psmShipMethods', psmShipMethods);
			}

			this.model.set('psmObj', this.createPsmObj(psmShipMethods));

			this.model.save().always(function()
			{
				self.clearError();
				self.step.enableNavButtons();
				jQuery.unblockUI();
			});
		}


		//object that replicates shermans psm obj
	,	createPsmObj: function (psmShipMethods)
		{
			var self = this;
			var psmObj = {
				'psms': {}
			,	'chargeableNumPsm': 0
			};

			_.each(psmShipMethods, function(shipMethod){
				psmObj = this.getPsmGroupInfo(shipMethod, psmObj);
			}, self);

			return psmObj;
		}

	,	getPsmGroupInfo: function(shipMethod, psmObj)
		{
			var sh = parseFloat(shipMethod.rateAndHandling);
			var cst = parseFloat(shipMethod.pacejetRate);
			var h = parseFloat(shipMethod.handling);
			var wt = shipMethod.weight;
			var code = '';
			var sdate = shipMethod.startDate;
			var carr = shipMethod.carrier;
			var via = parseFloat(shipMethod.shipMethodId);
			var seq = 0;

			var psmCode = shipMethod.psmCode;

			if (psmCode == 'PROD') {
				psmCode = 'blank';
			}

			psmObj['psms'][psmCode] = {};

			psmObj['psms'][psmCode].sh = sh;
			psmObj['psms'][psmCode].cst = cst;
			psmObj['psms'][psmCode].h = h;
			psmObj['psms'][psmCode].wt = wt;
			psmObj['psms'][psmCode].code = code;
			psmObj['psms'][psmCode].sdate = sdate;
			psmObj['psms'][psmCode].carr = carr;
			psmObj['psms'][psmCode].via = via;
			psmObj['psms'][psmCode].seq = seq;

			if (sh > 0) {
				psmObj['chargeableNumPsm'] += 1;
			}

			return psmObj;
		}

		//@method showError render the error message
	,	showError: function ()
		{

			var global_view_message = new GlobalViewsMessageView({
					message: this.error.errorMessage
				,	type: 'error'
				,	closable: true
			});

			// Note: in special situations (like in payment-selector), there are modules inside modules, so we have several place holders, so we only want to show the error in the first place holder.
			this.$('[data-type="alert-placeholder-module"]:first').html(
				global_view_message.render().$el.html()
			);

			this.error = null;

		}

		//check if the customer qualifies for free shipping based on seed rack and retailer free shipping thresholds
	,	checkFreeShipping: function(standardShipping, freeShipping)
		{
			var setFreeShipping = false;
			var profile = ProfileModel.getInstance();
			var priceLevelInfo = profile.getPriceLevelInfo();

			//minus shipping incase they have a fast or faster method selected that may be pushing the total over the threshold
			var orderTotal = parseFloat(this.model.get('summary').subtotal);

			if (freeShipping == 'T') {
				setFreeShipping = true;
			}
			//really only applys to seed rack customers, retailer free shipping is configured in the suitelet
			else if (priceLevelInfo.name == 'seedRack' && orderTotal >= priceLevelInfo.freeShippingThreshold) {
				setFreeShipping = true;
			}

			if (setFreeShipping == true) {
				standardShipping.handling = "0";
				standardShipping.handling_formatted = "$0.00";
				standardShipping.pacejetRate = "0";
				standardShipping.pacejetRate_formatted = "$0.00";
				standardShipping.rateAndHandling = "0";
				standardShipping.rateAndHandling_formatted = "$0.00";
			}

			return standardShipping;
		}

		//@method getContext @returns OrderWizard.Module.Shipmethod.Context
	,	getContext: function ()
		{
			var self = this
			,	show_enter_shipping_address_first = !this.model.get('isEstimating') && !this.profileModel.get('addresses').get(this.model.get('shipaddress'))
			,	shipping_methods = this.model.get('shipmethods').map(function (shipmethod)
				{
					return {
							name: shipmethod.get('name')
						,	rate_formatted: shipmethod.get('rate_formatted')
						,	internalid: shipmethod.get('internalid')
						,	isActive: shipmethod.get('internalid') === self.model.get('shipmethod')
					};
				});

			var psmShipGroups = this.model.get('psmShipGroups');
			var hasPsmShipGroups = (psmShipGroups != null && psmShipGroups.length > 0) ? true : false;
			if (hasPsmShipGroups) {
				for (var i = 0; i < psmShipGroups.length; i++) {
					if (psmShipGroups[i].standardShipping) {
						psmShipGroups[i].standardShipping = this.checkFreeShipping(psmShipGroups[i].standardShipping, psmShipGroups[i].freeShipping);
					}
				}
			}

			//loop through each psm group object and determine if a pacejet error occurred during the suitelet call
			var displayPacejetError = false;
			var pacejetError = '';
			if (hasPsmShipGroups){
				_.each(psmShipGroups, function(psmGroup){
					if (psmGroup.displayError == true) {
						displayPacejetError = true;
						pacejetError = psmGroup.errorMessage;
					}
				});
			}

			//@class OrderWizard.Module.Shipmethod.Context
			return {
					//@property {LiveOrder.Model} model
					model: this.model
					//@property {Boolean} showEnterShippingAddressFirst
				,	showEnterShippingAddressFirst: show_enter_shipping_address_first
					//@property {Boolean} showLoadingMethods
				,	showLoadingMethods: this.reloadingMethods
					//@property {Boolean} hasShippingMethods
				,	hasShippingMethods: !!shipping_methods.length
					//@property {Boolean} display select instead of radio buttons
				,	showSelectForShippingMethod: shipping_methods.length > 5
					//@property {Array} shippingMethods
				,	shippingMethods: shipping_methods
					//@property {Boolean} showTitle
				,	showTitle: !this.options.hide_title
					//@property {Straing} title
				,	title: this.options.title || _('Delivery Method').translate()
				,	hasPsmShipGroups: hasPsmShipGroups
				,	psmShipGroups: psmShipGroups
				,	displayPacejetError: displayPacejetError
				,	pacejetError: pacejetError
				,	containsServiceOnly: this.model.get('containsServiceOnly')
			};
		}
	});
});
