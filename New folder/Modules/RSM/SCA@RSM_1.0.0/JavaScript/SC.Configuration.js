/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module SC
// @class SC.Configuration
// All of the applications configurable defaults

define(
	'SC.Configuration'
,	[

		'item_views_option_tile.tpl'
	,	'item_views_option_text.tpl'
	,	'item_views_selected_option.tpl'

	,	'underscore'
	,	'Utils'
	]

,	function (

		item_views_option_tile_tpl
	,	item_views_option_text_tpl
	,	item_views_selected_option_tpl

	,	_
	)
{

	'use strict';

	var navigationDummyCategories = [
		{
			text: _('Jeans').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
				, hashtag: '#search'
			}
		},
		{
			text: _('Sweaters').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		},
		{
			text: _('Cardigan').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		},
		{
			text: _('Active').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		},
		{
			text: _('Shoes').translate()
		,	href: '/search'
		,	'class': 'header-menu-level3-anchor'
		,	data: {
				touchpoint: 'home'
			,	hashtag: '#search'
			}
		}
	];


	var Configuration = {


		// @property {Object} searchPrefs Search preferences
		searchPrefs:
		{
			// @property {Number} searchPrefs.maxLength Search preferences. keyword maximum string length - user won't be able to write more than 'maxLength' chars in the search box
			maxLength: 40

			// @property {Function} searchPrefs.keywordsFormatter Search preferences. Keyword formatter function will format the text entered by the user in the search box. This default implementation will remove invalid keyword characters like *()+-="
		,	keywordsFormatter: function (keywords)
			{
				if (keywords === '||')
				{
					return '';
				}

				var anyLocationRegex = /[\(\)\[\]\{\~\}\!\"\:\/]{1}/g // characters that cannot appear at any location
				,	beginingRegex = /^[\*\-\+]{1}/g // characters that cannot appear at the begining
				,	replaceWith = ''; // replacement for invalid chars

				return keywords.replace(anyLocationRegex, replaceWith).replace(beginingRegex, replaceWith);
			}
		}

		// @property {String} imageNotAvailable url for the not available image
	,	imageNotAvailable: _.getAbsoluteUrl('img/no_image_available.jpeg')

	,	templates: {
			itemOptions: {
				// each apply to specific item option types
				selectorByType:	{
					select: item_views_option_tile_tpl
				,	'default': item_views_option_text_tpl
				}
				// for rendering selected options in the shopping cart
			,	selectedByType: {
					'default': item_views_selected_option_tpl
				}
			}
		}

		// @class SCA.Shopping.Configuration
		// @property {Array<Object>} footerNavigation links that goes in the footer
	,	footerNavigation: [
			{text: 'Link a', href:'#'}
		,	{text: 'Link b', href:'#'}
		,	{text: 'Link c', href:'#'}
		]

		//string array of po box formats being rejected in address during checkout process
	,	invalidPoAddresses: [
			'po box'
		,	'post office box'
		,	'p.o. box'
		]

		//array of options not to be displayed on item details
	,	unwantedOptions: [
			'custcol_psm_group'
		,	'custcol_ship_date'
		,	'custcol_membership_type'
		,	'custcol_total_number_in_package'
		,	'custcol_sse_bulk_item_transform'
		,	'custcol_sse_so_tray_label'
        ,   'custcol_preferred_ship_method'
        ,   'custcol_sse_error_message'
        ,   'custcol5'
        ,   'custcol_special_wo_item'
        ,   'custcol_item_weight'
        ,   'custcol_item_weight_unit'
        ,   'custcol_sse_is_gift_item'
        ,   'custcol_ignore_backorder_pref'
        ,   'custcol_back_ordered_qty'
        ,   'custcol_sse_upc_code'
        ,   'custcol10'

		]

		//array of objects that contains price level information for for seed rack, and members
		//retailer is every other price level
		//member and retailer have same threshold since the free shipping functionality treats members and retailers has the same but for item display member an retailer are treated differently
	,	priceLevelsInfo: [
			{
				id: 2
			,	name: 'membership'
			,	freeShippingThreshold: 100
			}
		,	{
				id: 3
			,	name: 'seedRack'
			,	freeShippingThreshold: 50
			}
		,	{
				id: null //retailler is everyother price level besides seed rack, need seperate member object for the item display to work
			,	name: 'retailer'
			,	freeShippingThreshold: 100
			}
		]

		// @property {closable:Boolean,saveInCookie:Boolean,anchorText:String,message:String} cookieWarningBanner
		// settings for the cookie warning message (mandatory for UK stores)
	,	cookieWarningBanner: {
			closable: true
		,	saveInCookie: true
		,	anchorText: _('Learn More').translate()
		,	message: _('To provide a better shopping experience, our website uses cookies. Continuing use of the site implies consent.').translate()
		}

		// @class SCA.Shopping.Configuration
		// @property {betweenFacetNameAndValue:String,betweenDifferentFacets:String,betweenDifferentFacetsValues:String,betweenRangeFacetsValues:String,betweenFacetsAndOptions:String,betweenOptionNameAndValue:String,betweenDifferentOptions:String}
	,	facetDelimiters: {
			betweenFacetNameAndValue: '/'
		,	betweenDifferentFacets: '/'
		,	betweenDifferentFacetsValues: ','
		,	betweenRangeFacetsValues: 'to'
		,	betweenFacetsAndOptions: '?'
		,	betweenOptionNameAndValue: '='
		,	betweenDifferentOptions: '&'
		}
		// Output example: /brand/GT/style/Race,Street?display=table

		// eg: a different set of delimiters
		/*
		,	facetDelimiters: {
				betweenFacetNameAndValue: '-'
			,	betweenDifferentFacets: '/'
			,	betweenDifferentFacetsValues: '|'
			,	betweenRangeFacetsValues: '>'
			,	betweenFacetsAndOptions: '~'
			,	betweenOptionNameAndValue: '/'
			,	betweenDifferentOptions: '/'
		}
		*/
		// Output example: brand-GT/style-Race|Street~display/table

		// @param {Object} searchApiMasterOptions options to be passed when querying the Search API
	,	searchApiMasterOptions: {

			Facets: {
				include: 'facets'
			,	fieldset: 'search'
			}

		,	itemDetails: {
				include: 'facets'
			,	fieldset: 'details'
			}

		,	relatedItems: {
				fieldset: 'relateditems_details'
			}

		,	correlatedItems: {
				fieldset: 'correlateditems_details'
			}

			// don't remove, get extended
		,	merchandisingZone: {}

		,	typeAhead: {
				fieldset: 'typeahead'
			}

		,	itemsSearcher: {
				fieldset: 'itemssearcher'
			}
		}


		// @property {String} logoUrl header will show an image with the url you set here
	,	logoUrl: _.getAbsoluteUrl('img/SCA_Logo.png')

		// @property {String} defaultSearchUrl
	,	defaultSearchUrl: 'search'

		// @property {Boolean} isSearchGlobal setting it to false will search in the current results
		// if on facet list page
	,	isSearchGlobal: true

		// @property {#obj(minLength: Number, maxResults: Number, macro: String, sort: String)} typeahead Typeahead Settings
	,	typeahead: {
			minLength: 3
		,	maxResults: 7
		,	macro: 'typeahead'
		,	sort: 'relevance:asc'
		}

		// @property {Array<NavigationData>} NavigationData array of links used to construct navigation. (maxi menu and sidebar)
		// @class NavigationData
	, navigationData: [
		{
		    // @property {String} text
		    text: _('Home').translate()
		    // @property {String} href
		  , href: '/'
		    // @property {String} class
		  , 'class': 'header-menu-home-anchor'
		    // @property {touchpoint:String,hashtag:String} data
		  , data: {
		      touchpoint: 'home'
		    , hashtag: '#/'
		    }
		}
		,
		{
		    text: _('Shop').translate()
		  , href: '/search'
		  , 'class': 'header-menu-shop-anchor'
		  , categories: [
		    {
		        text: _('All Items').translate()
		      , href: '/search'
		      , 'class': 'header-menu-level3-anchor'
		      , data: {
		          touchpoint: 'home'
		        , hashtag: '#search'
		        }
		    },
		        {
		            text: _('Vegetable Seeds').translate()
		          , href: '/department/vegetable-seeds'
		          , 'class': 'header-menu-level3-anchor sub-dropdown'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#department/vegetable-seeds'
		            }, categories: [
		              {
		                  text: _('All Vegetable Seeds').translate()
		                ,   href: '/department/vegetable-seeds'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#department/vegetable-seeds'
		                  }
		               }
		               ,{
		                  text: _('Bean').translate()
		                ,   href: '/category/bean'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#category/bean'
		                  }
		               }
		              ,{
		                  text: _('Garlic').translate()
		                ,   href: '/category/garlic'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#category/garlic'
		                  }
		               }
		               ,{
		                  text: _('Lettuce').translate()
		                ,   href: '/category/lettuce'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#category/lettuce'
		                  }
		               }
		               ,{
		                  text: _('Pepper').translate()
		                ,   href: '/category/pepper'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#category/pepper'
		                  }
		               }
		               ,{
		                  text: _('Potatoes').translate()
		                ,   href: '/category/potatoes'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#category/potatoes'
		                  }
		               }
		               ,{
		                  text: _('Tomato').translate()
		                ,   href: '/category/tomato'
		                ,   data: {
		                      touchpoint: 'home'
		                    ,   hashtag: '#category/tomato'
		                  }
		               }
		          ]
		        },
		        {
		            text: _('Herb Seeds').translate()
		          , href: '/department/herb-seeds'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#department/herb-seeds'
		            }
		        }
		      ,
		      {
		          text: _('Flower Seeds').translate()
		        , href: '/department/flower-seeds'
		        , 'class': 'header-menu-level3-anchor sub-dropdown'
		        , data: {
		            touchpoint: 'home'
		          , hashtag: '#department/flower-seeds'
		          }
		          , categories: [
		            {
		                text: _('All Flower Seeds').translate()
		              ,   href: '/department/flower-seeds'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#department/flower-seeds'
		                }
		             }
		          ,
		            {
		                text: _('Flowers').translate()
		              ,   href: '/category/flowers'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/flowers'
		                }
		             }
		          ,  {
		                text: _('Prairie Flowers & Grasses').translate()
		              ,   href: '/category/prairie'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/prairie'
		                }
		             }
		           , {
		                text: _('Sunflowers').translate()
		              ,   href: '/category/sunflowers'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/sunflowers'
		                }
		             }
		        ]
		      },
		      {
		          text: _('Organic Seeds').translate()
		        , href: '/organic/organic'
		        , 'class': 'header-menu-level3-anchor sub-dropdown'
		        , data: {
		            touchpoint: 'home'
		          , hashtag: '#organic/organic'
		          }
		          , categories: [
		            {
		                text: _('All Organic Seeds').translate()
		              ,   href: '/organic/organic'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#organic/organic'
		                }
		             }
		          ,
		            {
		                text: _('Vegetables').translate()
		              ,   href: '/department/vegetable-seeds/organic/organic'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#department/vegetable-seeds/organic/organic'
		                }
		             }
		          ,  {
		                text: _('Herbs').translate()
		              ,   href: '/department/herb-seeds/organic/organic'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#department/herb-seeds/organic/organic'
		                }
		             }
		           , {
		                text: _('Flowers').translate()
		              ,   href: '/department/flower-seeds/organic/organic'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#department/flower-seeds/organic/organic'
		                }
		             }
		        ]
		      }
		      ,
		      {
		        text: _('New Items').translate()
		      ,   href: '/special/new-items'
		      , 'class': 'header-menu-level3-anchor'
		      ,   data: {
		            touchpoint: 'home'
		          ,   hashtag: '#special/new-items'
		        }
		      }
		    ,   {
		        text: _('On Sale').translate()
		      ,   href: '/special/on-sale'
		      , 'class': 'header-menu-level3-anchor'
		      ,   data: {
		            touchpoint: 'home'
		          ,   hashtag: '#special/on-sale'
		        }
		      }
		    ,   {
		        text: _('Online Exclusives').translate()
		      ,   href: '/special/online-exclusives'
		      , 'class': 'header-menu-level3-anchor'
		      ,   data: {
		            touchpoint: 'home'
		          ,   hashtag: '#special/online-exclusives'
		        }
		      }
		      ,
		      {
		          text: _('Trees & Transplants').translate()
		        ,   href: '/department/trees-transplants'
		        , 'class': 'header-menu-level3-anchor sub-dropdown'
		        ,   data: {
		              touchpoint: 'home'
		            ,   hashtag: '#department/trees-transplants'
		          }
		          , categories: [
		            {
		                text: _('All Trees & Transplants').translate()
		              ,   href: '/department/trees-transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#department/trees-transplants'
		                }
		             }
		          ,
		            {
		                text: _('All Apple Trees').translate()
		              ,   href: '/category/apple-trees'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/apple-trees'
		                }
		             }
		          ,  {
		                text: _('All Transplants').translate()
		              ,   href: '/category/transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/transplants'
		                }
		             }
		          ,  {
		                text: _('Pepper Transplants').translate()
		              ,   href: '/quick_find/pepper-transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#quick_find/pepper-transplants'
		                }
		             }
		          ,  {
		                text: _('Tomato Transplants').translate()
		              ,   href: '/quick_find/tomato-transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#quick_find/tomato-transplants'
		                }
		             }
		          ,  {
		                text: _('Herb Transplants').translate()
		              ,   href: '/quick_find/herb-transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#quick_find/herb-transplants'
		                }
		             }
		          ,  {
		                text: _('Ground Cherry Transplants').translate()
		              ,   href: '/quick_find/ground-cherry-transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#quick_find/ground-cherry-transplants'
		                }
		             }
		          ,  {
		                text: _('Transplant Samplers').translate()
		              ,   href: '/quick_find/sampler-transplants'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#quick_find/sampler-transplants'
		                }
		             }
		        ]
		        }
		        ,
		        {
		            text: _('Gift Shop').translate()
		          ,   href: '/department/gift-shop'
		          , 'class': 'header-menu-level3-anchor sub-dropdown'
		          ,   data: {
		                touchpoint: 'home'
		              ,   hashtag: '#department/gift-shop'
		            }
		            , categories: [
		              {
		               text: _('All Gift Shop Items').translate()
		             ,   href: '/department/gift-shop'
		             ,   data: {
		                   touchpoint: 'home'
		                 ,   hashtag: '#department/gift-shop'
		               }
		            }
		           ,
		               {
		                text: _('Apparel').translate()
		              ,   href: '/category/apparel'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/apparel'
		                }
		             }
		            ,   {
		                text: _('Books').translate()
		              ,   href: '/category/books'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/books'
		                }
		             }
		           , {
		                text: _('Decor').translate()
		              ,   href: '/category/decor'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/decor'
		                }
		             }
		           , {
		                text: _('Gift Cards').translate()
		              ,   href: '/category/gift-certificates'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/gift-certificates'
		                }
		             }
		           , {
		                text: _('Kitchen').translate()
		              ,   href: '/category/kitchen'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/kitchen'
		                }
		             }
		           , {
		                text: _('Seed Collection').translate()
		              ,   href: '/category/seed-collections'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/seed-collections'
		                }
		             }

		           , {
		                text: _('Seed Saving Tools').translate()
		              ,   href: '/category/seed-saving-tools'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#category/seed-saving-tools'
		                }
		             }

		          ]
		          }
		          ,
		            {
		                text: _('Retail Seed Locations').translate()
		              ,   href: '/seed-rack-locations'
		              , 'class': 'header-menu-level3-anchor sub-dropdown'
		              ,   data: {
		                    touchpoint: 'home'
		                  ,   hashtag: '#seed-rack-locations'
		                }
		              , categories: [
			              {
			               text: _('Find Retail Partners').translate()
			             ,   href: '/seed-rack-locations'
			             ,   data: {
			                   touchpoint: 'home'
			                 ,   hashtag: '#seed-rack-locations'
			               }
			            }
			           ,
								 {
									text: _('Become A Retailer').translate()
								,   href: '/seed-rack'
								,   data: {
											touchpoint: 'home'
										,   hashtag: '#seed-rack'
									}
							 }
						 ]
					 }
		    ]
		}
		,
		{
		    text: _('Support').translate()
		  , href: '/support'
		  , 'class': 'header-menu-shop-anchor'
		  , categories: [
		    {
		        text: _('Membership').translate()
		      , href: '/join'
		      , 'class': 'header-menu-level3-anchor sub-dropdown'
		      , categories: [
		            {
		                text: _('Join').translate()
		              , href: '/join'
		              , data: {
		                  touchpoint: 'home'
		                , hashtag: '#join'
		                }
		            },
		            {
		                text: _('Renew').translate()
		              , href: '/renew'
		              , data: {
		                  touchpoint: 'home'
		                , hashtag: '#renew'
		                }
		            },
		            {
		                text: _('Gift').translate()
		              , href: '/search?keywords=gift membership'
		              , data: {
		                  touchpoint: 'home'
		                , hashtag: '#search?keywords=gift membership'
		                }
		            }
		        ]
		    },
		    {
		        text: _('Donate').translate()
		      , href: '/donate'
		      , 'class': 'header-menu-level3-anchor'
		      , data: {
		          touchpoint: 'home'
		        , hashtag: '#donate'
		        }
		    },
		    {
		        text: _('Volunteer').translate()
		      , href: '/volunteer'
		      , 'class': 'header-menu-level3-anchor'
		      , data: {
		          touchpoint: 'home'
		        , hashtag: '#volunteer'
		        }
		    }

		    ]
		}
		,
		{
		    text: _('Exchange').translate()
		  , href: 'http://exchange.seedsavers.org'
			, target: '_blank'
		  , 'class': 'header-menu-shop-anchor'
		  , data: {
		      touchpoint: 'home'
		        }
		}
		,
		{
		    text: _('Resources').translate()
		  , href: '/resources'
		  , 'class': 'header-menu-shop-anchor'
		  , categories: [
		        {
		            text: _('Learn').translate()
		          , href: '/learn'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#learn'
		            }
		        },
		        {
		            text: _('Withee Exhibit').translate()
		          , href: '/withee-exhibit'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#withee-exhibit'
		            }
		        },
		        {
		            text: _('Community Seed Resources').translate()
		          , href: '/csrp'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#csrp'
		            }
		        },
		        {
		            text: _('Seed Packet Donation').translate()
		          , href: '/seed-donation-program'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#seed-donation-program'
		            }
		        }
		    ]
		}
		,
		{
		    text: _('Visit').translate()
		  , href: '/visit'
		  , 'class': 'header-menu-shop-anchor'
		  , categories: [
		        {
		            text: _('Our Farm').translate()
		          , href: '/visit'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#visit'
		            }
		        },
		        {
		            text: _('Our Library').translate()
		          , href: '/library'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#library'
		            }
		        },
		        ,
		          {
		              text: _('Events').translate()
		            , href: '/events'
		            , 'class': 'header-menu-level3-anchor sub-dropdown'
		            , categories: [
		                  {
		                      text: _('All Events').translate()
		                    , href: '/events'
		                    , data: {
		                        touchpoint: 'home'
		                      , hashtag: '#events'
		                      }
		                  },
		                  {
		                      text: _('Winter on the Farm').translate()
		                    , href: '/winteronthefarm'
		                    , data: {
		                        touchpoint: 'home'
		                      , hashtag: '#winteronthefarm'
		                      }
		                  }
		              ]
		          }
		    ]
		}
		,
		{
		    text: _('About Us').translate()
		  , href: '/mission'
		  , 'class': 'header-menu-shop-anchor'
		  , categories: [
		        {
		            text: _('Our Mission').translate()
		          , href: '/mission'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#mission'
		            }
		        },
		        {
		            text: _('Our Story').translate()
		          , href: '/story'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#story'
		            }
		        },
		        {
		            text: _('Our Leadership').translate()
		          , href: '/leadership'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#leadership'
		            }
		        },
		        {
		            text: _('Financial Information').translate()
		          , href: '/financial'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#financial'
		            }
		        },
		        {
		            text: _('Annual Report').translate()
		          , href: '/annual-report'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#annual-report'
		            }
		        }
						,
		        {
		            text: _('Job Opportunities').translate()
		          , href: '/jobs'
		          , 'class': 'header-menu-level3-anchor'
		          , data: {
		              touchpoint: 'home'
		            , hashtag: '#jobs'
		            }
		        }
		    ]
		}

	]

		// @property {Object} imageSizeMapping map of image custom image sizes
		// usefull to be customized for smaller screens
	,	imageSizeMapping: {
			thumbnail: 'thumbnail' // 175 * 175
		,	main: 'main' // 600 * 600
		,	tinythumb: 'tinythumb' // 50 * 50
		,	zoom: 'zoom' // 1200 * 1200
		,	fullscreen: 'fullscreen' // 1600 * 1600
		,	homeslider: 'homeslider' // 200 * 220
		,	homecell: 'homecell' // 125 * 125
		}

		// @property {Array} paymentmethods map of payment methods, please update the keys using your account setup information.

	,	paymentmethods: [
			{
				key: '5,5,1555641112' //'VISA'
			,	regex: /^4[0-9]{12}(?:[0-9]{3})?$/
			}
		,	{
				key: '4,5,1555641112' //'Master Card'
			,	regex: /^5[1-5][0-9]{14}$/
			}
		,	{
				key: '6,5,1555641112' //'American Express'
			,	regex: /^3[47][0-9]{13}$/
			}
		,	{
				key: '3,5,1555641112' // 'Discover'
			,	regex: /^6(?:011|5[0-9]{2})[0-9]{12}$/
			}
		,	{
				key: '16,5,1555641112' // 'Maestro'
			,	regex: /^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/
			}
		,	{
				key: '17,3,1555641112' // External
			,	description: 'This company allows both private individuals and businesses to accept payments over the Internet'
			}
		]

	,	bxSliderDefaults: {
			minSlides: 2
		,	slideWidth: 228
		,	maxSlides: 5
		,	forceStart: true
		,	pager: false
		,	touchEnabled:true
		,	nextText: '<a class="item-details-carousel-next"><span class="control-text">' + _('next').translate() + '</span> <i class="carousel-next-arrow"></i></a>'
		,	prevText: '<a class="item-details-carousel-prev"><i class="carousel-prev-arrow"></i> <span class="control-text">' + _('prev').translate() + '</span></a>'
		,	controls: true
		,	preloadImages: 'all'
		}

	,	siteSettings: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.siteSettings || {}

	,	tracking: {
			googleTagManager: {
				id: 'GTM-P8C6LS'
			,	dataLayerName: 'dataLayer'
			}
		}

	,	get: function (path, defaultValue)
		{
			return _.getPathFromObject(this, path, defaultValue);
		}

	,	getRegistrationType: function ()
		{
    		//registrationmandatory is 'T' when customer registration is disabled
			if (Configuration.get('siteSettings.registration.registrationmandatory') === 'T')
			{
				// no login, no register, checkout as guest only
				return 'disabled';
			}
			else
			{
				if (Configuration.get('siteSettings.registration.registrationoptional') === 'T')
				{
					// login, register, guest
					return 'optional';
				}
				else
				{
					if (Configuration.get('siteSettings.registration.registrationallowed') === 'T')
					{
						// login, register, no guest
						return 'required';
					}
					else
					{
						// login, no register, no guest
						return 'existing';
					}
				}
			}
		}
	};

	// Append Product Lists configuration
	_.extend(Configuration, {
		product_lists: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.PRODUCTLISTS_CONFIG
	});

	// Append Cases configuration
	_.extend(Configuration, {
		cases: {
			config: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.CASES_CONFIG
		,	enabled: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.casesManagementEnabled
		}
	});

	_.extend(Configuration, {
		useCMS: SC && SC.ENVIRONMENT && SC.ENVIRONMENT.useCMS
	});

	// Append Bronto Integration configuration
	_.extend(Configuration, {
		bronto: {
			accountId: ''
		}
	});

	return Configuration;
});
