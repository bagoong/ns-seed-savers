/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @Module OrderWizard.Module.SubmitButton
define(
	'OrderWizard.Module.SubmitButton'
, [
		'Wizard.Module'
	, 'Profile.Model'
	, 'order_wizard_submitbutton_module.tpl'
	, 'underscore'
	, 'Utils'

]
, function (

		WizardModule
	, ProfileModel
	, order_wizard_submitbutton_module_tpl
	, _

	) {
    'use strict';

    // @class OrderWizard.Module.SubmitButton @extends WizardModule
    return WizardModule.extend({

        template: order_wizard_submitbutton_module_tpl

	, render: function () {
	    this._render();
	    this.trigger('ready', true);
	}

        // @method getContinueButtonLabel @returns {String}
	, getContinueButtonLabel: function () {
	    var current_step = this.wizard.getCurrentStep()
        , label = _('Place Order').translate();

	    if (current_step) {
	        label = current_step.getContinueButtonLabel();
	    }

	    return label;
	}


        // @method getContext @return OrderWizard.Module.SubmitButton.Context
	, getContext: function () {
	    var self = this;
	    var totalCount = 0;
	    var current_step = this.wizard.getCurrentStep();
	    var step = current_step.options.stepGroup.name;
	    var profile = ProfileModel.getInstance();
	    var enforce250 = profile.get('enforce250');
	    var disableSubmit = false;
	    if (step == 'Review' && enforce250 == 'T') {
	        var lines = this.model.get('lines');
	        _.each(lines.models, function (model) {
	            var item = model.get('item');
	            var qty = item.get('quantity');
	            var size = item.get('custitem_total_number_in_package');

	            if (size && !isNaN(size)){
	                size = qty * size;
	            }

	            totalCount += size;
	        }, self);

	        if (totalCount < 250)
	            disableSubmit = true;
	    }
	    // @class OrderWizard.Module.SubmitButton.Context
	    return {
	        //@property {Boolean} showWrapper
	        showWrapper: !!this.options.showWrapper
	        //@property {String} wrapperClass
        , wrapperClass: this.options.wrapperClass
	        //@property {String} continueButtonLabel
        , continueButtonLabel: this.getContinueButtonLabel() || ''
	        //@property {String} continueButtonLabel
        , disableButton: disableSubmit
        , showError: disableSubmit
	    };
	}
    });
});
