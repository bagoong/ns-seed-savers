{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showWrapper}}
<div class="{{wrapperClass}}">
{{#if showError}}
		<p data-validation-error="block">{{translate 'You must have at least 250 seeds in your cart to checkout'}}</p>
{{/if}}
{{/if}}
<button class="order-wizard-submitbutton-module-button" data-action="submit-step" {{#if disableButton}}disabled{{/if}}>{{continueButtonLabel}}</button>
{{#if showWrapper}}
</div>
{{/if}}
