/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrder
define(
		'QuickOrder.Model'
,	[
		'Transaction.Model'

	,	'LiveOrder.Model'
	,	'SC.Shopping.Configuration'

	,	'underscore'
	,	'jQuery'
	,	'blockui'
	,	'Utils'
	]
,	function (
		TransactionModel

	,	LiveOrderModel
	,	Configuration

	,	_
	,	jQuery
	)
{
	'use strict';

	//@class QuickOrder.Model @extends Transaction.Model
	return TransactionModel.extend({

		//@property {String} urlRoot
		urlRoot: 'services/Quote.Service.ss?recordtype=estimate'

		//@method submit Method invoked when the user a new QuickOrder want to be created
		// @return {jQuery.Deferred}
	,	submit: function ()
		{
			// var self = this;
			// return this._submit.apply(this, arguments)
			// 	.fail(function (jqXhr)
			// {
			// 		jqXhr.preventDefault = true;
			// 	})
			// 	.done(function(){
			// 		self.trigger('submit');
			// });
			return this.addItemsToCart();
		}

	,  addItemsToCart: function ()
		{
			var itemsToBeAdded = [];
			_.each(this.get('lines').models, function(line){
				itemsToBeAdded.push(line.get('item'));
			});

			_.each(itemsToBeAdded, function(item){
				var qty = item.get('quantity');
				item.setOption('quantity', qty);
			});

			var self = this
			,	cart = LiveOrderModel.getInstance()
			//,	layout = this.application.getLayout()
			,	cart_promise
			,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate()
			,	blockUiMessage = itemsToBeAdded.length > 1 ? '<h1>Adding Items To The Cart...</h1>' : '<h1>Adding Item To The Cart...</h1>';

			jQuery.blockUI({
				message: blockUiMessage,
				css: {
					width: '100%',
					left: ''
				}
			});

			cart_promise = cart.addItems(itemsToBeAdded).done(function ()
			{
				cart.set('multiRecentlyAdded', itemsToBeAdded);
				//If there is an item added into the cart
				if (cart.getLatestAddition())
				{
					jQuery.unblockUI();
					self.trigger('submit');
				}
				else
				{
					jQuery.unblockUI();
					self.showError(error_message);
				}
			});

			return cart_promise;
		}

		//@method save Override default save method to validate that independently of the current wizard steps configuration
		//the default QuickOrder values are preserve
		//@return {jQuery.XHR|Boolean} jqXHR if validation is successful and false
	,	save: function ()
		{
			var billing_address = parseInt(this.get('billaddress'), 10)
			,	shipping_address = parseInt(this.get('shipaddress'), 10);

			//We don't know if the shipping address module is for sure present, so we add here an extra validation
			//to just save a valid shipping address and not an id that was auto-generated in the back-end
			if (!_.isNumber(shipping_address) || _.isNaN(shipping_address))
			{
				this.unset('shipaddress');
			}

			//We don't know if the billing address module is for sure present, so we add here an extra validation
			//to just save a valid billing address and not an id that was auto-generated in the back-end
			if (!_.isNumber(billing_address) || _.isNaN(billing_address))
			{
				this.unset('billaddress');
			}

			//Fix terms payment method object name
			//This happens because in some cases the back-end returns a payment method already initialized
			//that can be terms depending on the current user.
			//Our Transaction.Model returns the invoice in an object with the property 'paymentterms'
			//but at the time to set the payment method it expect an object with a property 'terms'
			if (this.get('paymentmethods').length)
			{
				var term_paymentMethod = this.get('paymentmethods').find(function (payment_method)
					{
						return !!payment_method.get('paymentterms');
					});

				if (term_paymentMethod)
				{
					term_paymentMethod.set('terms', term_paymentMethod.get('paymentterms'));
					term_paymentMethod.unset('paymentterms');
				}
			}

			return TransactionModel.prototype.save.apply(this, arguments);
		}

		// @method getNonShippableLines Returns the order's line that are NON Shippable
		// @returns {Array<Transaction.Line.Model>}
	,	getNonShippableLines: function ()
		{
			return this.get('lines').filter(function (line)
				{
					return !line.get('item').get('_isfulfillable');
				});
		}

		//@method shippingAddressIsRequired This method is used by the OrderWizard.Module.Address.Shipping to determine if its must be rendered or not.
		// @return {Boolean}
	,	shippingAddressIsRequired: function ()
		{
			return true;
			// return this.get('lines').length ?
			// 	this.getNonShippableLines().length !== this.get('lines').length :
			// 	true;
		}
	});
});
