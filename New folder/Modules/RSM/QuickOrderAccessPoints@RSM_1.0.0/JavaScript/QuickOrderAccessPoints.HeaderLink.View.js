/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module QuickOrderAccessPoints
define('QuickOrderAccessPoints.HeaderLink.View'
,	[
		'quickorder_accesspoints_headerlink.tpl'

	,	'Backbone'
	]
,	function (
		quickorder_accesspoints_headerlink_tpl

	,	Backbone
	)
{
	'use strict';

	//@class QuickOrderAccessPoints.HeaderLink.View @extend Backbone.View
	return Backbone.View.extend({

		//@property {Function} template
		template: quickorder_accesspoints_headerlink_tpl

		//@method getContext
		//@return {QuickOrderAccessPoints.HeaderLink.View.Context}
	, 	getContext: function ()
		{
			//@class QuickOrderAccessPoints.HeaderLink.View.Context
			return {
				//@property {Boolean} hasClass
				hasClass: !!this.options.className
				//@property {String} className
			,	className: this.options.className
			};
			//@class QuickOrderAccessPoints.HeaderLink.View
		}
	});
});
