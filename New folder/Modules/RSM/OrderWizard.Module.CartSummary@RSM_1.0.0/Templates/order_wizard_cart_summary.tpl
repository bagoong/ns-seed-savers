{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="order-wizard-cart-summary-container">
		<h3 class="order-wizard-cart-summary-title">
			{{translate 'Order Summary'}}
		</h3>

	<div class="order-wizard-cart-summary-body">
		{{#if showEditCartMST}}
		<div class="order-wizard-cart-summary-edit-cart-label-mst">
			<a href="#" class="order-wizard-cart-summary-edit-cart-link" data-touchpoint="viewcart">
				{{translate 'Edit Cart'}}
			</a>
		</div>
		{{/if}}
		<div class="order-wizard-cart-summary-subtotal">
			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-grid-right" >
					{{model.summary.subtotal_formatted}}
				</span>
				<span class="order-wizard-cart-summary-subtotal-label">
					{{#if itemCountGreaterThan1}}
						{{translate 'Subtotal <span class="order-wizard-cart-summary-item-quantity-subtotal" data-type="cart-summary-subtotal-count">$(0) items</span>' itemCount}}
					{{else}}
						{{translate 'Subtotal <span class="order-wizard-cart-summary-item-quantity-subtotal" data-type="cart-summary-subtotal-count">$(0) item</span>' itemCount}}
					{{/if}}
				</span>
			</p>
		</div>


		{{#if showPromocode}}
			<div class="order-wizard-cart-summary-promocode-applied">
				<p class="order-wizard-cart-summary-promo-code-applied">
					{{translate 'Promo Code Applied'}} {{model.summary.discountrate_formatted}}
				</p>
				<div class="order-wizard-cart-summary-grid">
					<div class="order-wizard-cart-summary-promocode-text-success">
						<span class="order-wizard-cart-summary-promocode-code">#{{model.promocode.code}} - {{translate 'Instant Rebate'}}</span>
					{{#if showRemovePromocodeButton}}
						<a href="#" data-action="remove-promocode">
							<span class="order-wizard-cart-summary-remove-container">
								<i class="order-wizard-cart-summary-remove-icon"></i>
							</span>
						</a>
					{{/if}}
			</div>
				</div>
			</div>
		{{/if}}

		{{#if showDiscount}}
			<div class="order-wizard-cart-summary-discount-applied">
				<p class="order-wizard-cart-summary-grid-float">
					<span class="order-wizard-cart-summary-discount-total">
						{{model.summary.discounttotal_formatted}}
					</span>
					{{translate 'Discount Total'}}
				</p>
			</div>
		{{/if}}

		{{#if showGiftCertificates}}
			<div class="order-wizard-cart-summary-giftcertificate-applied">
				<p class="order-wizard-cart-summary-gift-certificate">{{translate 'Amount Applied ($(0))' giftCertificates.length}}</p>
				<div data-view="GiftCertificates"></div>
			</div>
		{{/if}}
		<div class="order-wizard-cart-summary-shipping-cost-applied">
		<!-- {{#if psmShipMethods}}
		{{#each psmShipMethods}}
		<p class="order-wizard-cart-summary-grid-float">
			<span class="order-wizard-cart-summary-shipping-cost-formatted">
				{{rateAndHandling_formatted}}
			</span>
			{{psmName}}
		</p>
		{{/each}}
		{{/if}} -->

			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-shipping-cost-formatted">
					{{model.summary.shippingcost_formatted}}
				</span>
				{{translate 'Shipping & Handling'}}
			</p>

			{{#if showHandlingCost}}
				<p class="order-wizard-cart-summary-grid-float">
					<span class="order-wizard-cart-summary-handling-cost-formatted">
						{{model.summary.handlingcost_formatted}}
					</span>
					{{translate 'Handling'}}
				</p>
			{{/if}}
			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-tax-total-formatted" >
					{{model.summary.taxtotal_formatted}}
				</span>
				{{translate 'Tax'}}
			</p>
		</div>

		<div class="order-wizard-cart-summary-total">
			<p class="order-wizard-cart-summary-grid-float">
				<span class="order-wizard-cart-summary-grid-right" >
					{{model.summary.total_formatted}}
				</span>
				{{translate 'Total'}}
			</p>
		</div>
		{{#if showWarningMessage}}
		<div class="order-wizard-cart-summary-warning" role="alert">
			<div>{{warningMessage}}</div>
</div>
	{{/if}}

		<div class="container-fluid" >
			<div class="row">
				<div class="col-xs-12">
				<p>Support our nonprofit mission to save and preserve America's heirloom seeds.
Add a donation to your purchase:</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4">
					<a class="button-secondary button-small button-full" title="Donate" onclick="javascript:window.location = '/app/site/backend/additemtocart.nl?buyid=34736&amp;quantity=1'">$7</a>
				</div>
				<div class="col-xs-4">
					<a class="button-secondary button-small button-full" title="Donate" onclick="javascript:window.location = '/app/site/backend/additemtocart.nl?buyid=25865&amp;quantity=1'" style="wdith:100%">$5</a>
				</div>
				<div class="col-xs-4">
					<a class="button-secondary button-small button-full" title="Donate" onclick="javascript:window.location = '/app/site/backend/additemtocart.nl?buyid=25830&amp;quantity=1'">$2</a>
				</div>
			</div>
		</div>

	</div>
		</div>
