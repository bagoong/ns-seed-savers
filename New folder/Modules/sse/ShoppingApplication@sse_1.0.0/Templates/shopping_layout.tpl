{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div id="layout" class="shopping-layout">
	<header id="site-header" class="shopping-layout-header" data-view="Header"></header>
	<div id="main-container">
		<!--Add breadcrumbs back here if you decide to do so.-->
		<!-- Main Content Area -->
		<div id="content" class="shopping-layout-content"></div>
		<!-- / Main Content Area -->
	</div>
	<footer id="site-footer" class="shopping-layout-footer" data-view="Footer"></footer>
</div>
