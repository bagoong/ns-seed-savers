{{! © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code; provided, however, if you are an authorized user with a NetSuite account or log-in, you may use this code subject to the terms that govern
your access and use. }}

<div class="home">

	<!-- <div class="red-banner-home">
		<a href="/donate"><p class="red-banner-txt-home">Your support matters. Contribute to the Annual Fund today.</p></a>
	</div> -->

	<a target="_blank" href="/donate">
		<div class="red-banner-home-alt">
			<div class="container-normal">
				<div class="row red-bar-row">
					<div class="red-bar-txt-all col-sm-9">
						<p class="red-banner-txt-home-a">Your support matters</p>
						<p class="red-banner-txt-home-b">Contribute to the Annual Fund today</p>
					</div>
					<div class="red-bar-button-col col-sm-3 ">
						<span class="red-bar-button button"><span>Donate Now</span></span>
					</div>
				</div>
			</div>
		</div>
	</a>

	<div class="home-jumbotron">
		<div class="container">
			<div class="row home-jumbotron-content">
				<div class="col-md-6">
					<h1>Grow Heirloom Seeds</h1>
					<p>
						Protect America's garden heritage by growing seeds with a story.
					</p>
					<!-- <a href="/search" class="home-shop-button">Shop our seeds</a> -->
					<div class="home-three-buttons">
						<a href="/search" class="banner-buttons">Shop</a>
						<a href="/donate" class="banner-buttons">Donate</a>
						<a href="/join" class="banner-buttons">Join</a>
					</div>
					<p>
						<a class="base-text" href="/mission">Learn more about our nonprofit mission.</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="" class=" shop-box">
		<div class="container">
			<div class="row">
				<a target="_blank" href="/gardening-gifts">
					<div class="box-one col-sm-4">
						<div class="overlay-img-block box-img-one">
							<div class="overlay-text-block">
								<div class="overlay-text-subtitle">
									Featured
								</div>
								<div class="overlay-text-title">
									Holiday Gift Guide
								</div>
							</div>
						</div>
					</div>
				</a>
				<a target="_blank" href="/department/vegetable-seeds">
					<div class="box-a col-sm-4">
						<div class="overlay-img-block box-img-a">
							<div class="overlay-text-block">
								<div class="overlay-text-subtitle">
									Shop
								</div>
								<div class="overlay-text-title">
									Vegetable Seeds
								</div>
							</div>
						</div>
					</div>
				</a>
				<a target="_blank" href="/department/flower-seeds">
					<div class="box-b  col-sm-4">
						<div class="overlay-img-block box-img-b">
							<div class="overlay-text-block">
								<div class="overlay-text-subtitle">
									Shop
								</div>
								<div class="overlay-text-title">
									Flower Seeds
								</div>
							</div>
						</div>
					</div>
				</a>
				<a target="_blank" href="/department/herb-seeds">
					<div class="box-c col-sm-4">
						<div class="overlay-img-block box-img-c">
							<div class="overlay-text-block">
								<div class="overlay-text-subtitle">
									Shop
								</div>
								<div class="overlay-text-title">
									Herb Seeds
								</div>
							</div>
						</div>
					</div>
				</a>
				<a target="_blank" href="/donate">
					<div class="box-d  col-sm-4">
						<div class="overlay-img-block box-img-d">
							<div class="overlay-text-block">
								<div class="overlay-text-subtitle">
									Your Support Matters
								</div>
								<div class="overlay-text-title">
									Donate
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
<!-- 
	<div class="home-shop">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2>Buy organic, heirloom, and non-hybrid seeds.</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a class="buyintro-btn" href="/department/vegetable-seeds">
						<img src="http://sandbox.seedsavers.org/site/assets/black-valentine-bean.jpg" alt="Vegetable Seeds" />
						<p>Vegetable Seeds</p>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a class="buyintro-btn" href="/department/flower-seeds">
						<img src="http://sandbox.seedsavers.org/site/assets/grandpa-otts-morning-glory.jpg" alt="Flower Seeds" />
						<p>Flower Seeds</p>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a class="buyintro-btn" href="/department/herb-seeds">
						<img src="http://sandbox.seedsavers.org/site/assets/english-lavender.jpg" alt="Herb Seeds" />
						<p>Herb Seeds</p>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a class="buyintro-btn" href="/gardening-gifts">
						<img src="/site/img/seedcollection.jpg" alt="Holiday Gifts" />
						<p style="font-size: 21px;">Holiday Gifts</p>
					</a>
				</div>
			</div>
		</div>
	</div> -->

	<div class="home-work">
		<div class="container">
			<div class="row">
				<div class="col-sm-12" style="text-align: center;">
					<h2 style="color: #1f2223 !important; padding: 0px 0px 40px 0px;">What Seed Savers Exchange Does</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 col-xs-6">
					<p class="img-banner-txt">
						We keep seeds safe in our seed vault.
					</p>
				</div>
				<div class="col-md-2 col-xs-6">
					<img class="img-banner" src="http://www.seedsavers.org/site/assets/home-safe-icon.png" alt="Vault" />
				</div>
				<div class="col-md-2 col-xs-6 ">
					<p class="img-banner-txt">
						We help others to save and share seeds.
					</p>
				</div>
				<div class="col-md-2 col-xs-6">
					<img class="img-banner" src="http://www.seedsavers.org/site/assets/home-sse-icon.png" alt="Share" />
				</div>
				<div class="col-md-2 col-xs-6">
					<p class="img-banner-txt">
						We sell seeds through our catalog.
					</p>
				</div>
				<div class="col-md-2 col-xs-6">
					<img class="img-banner" src="http://www.seedsavers.org/site/assets/home-packet-icon.png" alt="Catalog" />
				</div>
			</div>
		</div>
	</div>

	<div class="home-video">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="cms-video">
						<iframe style="position:absolute" src="https://www.youtube.com/embed/3CDEq9oww60" allowfullscreen="" width="100%" frameborder="0" height="100%"></iframe>
					</div>
				</div>
				<div class="col-md-6">
					<h2>Join Our Mission</h2>
					<p>
						Find out why it's so important to grow heirloom seeds.
					</p>
					<a class="home-event-button" href="/mission">Our Mission</a>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="home-event">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<img src="http://www.seedsavers.org/site/img/50salea_update.jpg" alt="End of Season Sale" />
				</div>
				<div class="col-md-6">
					<h2>End of Season Sale</h2>
					<p>
						Stock up on seeds for next spring and save! Select seed packets are on sale now. Don't miss your chance to get a deal on these heirloom seeds.
					</p>
					<a class="home-event-button" href="/special/on-sale">Shop Now</a>
				</div>
			</div>
		</div>
	</div> -->

	<div class="home-quote-container">
		<div class="container home-quote">
			<h2>
				"Institutions do not save seeds. Humans with hearts do..."
			</h2>
			<p>
				-Gary Nabhan
			</p>
			<a href="/join" class="button-primary button-large join-today-btn" style="font-size: 18px;">Join Today</a>
		</div>
	</div>

	<div class="home-merchandizing-zone">
		<div data-id="your-merchandising-zone" data-type="merchandising-zone"></div>
	</div>

</div>
