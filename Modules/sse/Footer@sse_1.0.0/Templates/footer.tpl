{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div data-view="Global.BackToTop"></div>

<div class="footer-support">
	<div class="container">
		<p>As a customer, member, or donor,  you help us save America’s heirloom seeds.</p>
	</div>
</div>

<div class="footer-wood">
</div>

<div class="footer-content">

	<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3>Stay Connected</h3>
				<a href="https://twitter.com/seedsaversx"><i class="twitter-icon"></i></a>
				<a href="https://www.facebook.com/seedsaversx"><i class="facebook-icon"></i></a>
				<a href="https://www.pinterest.com/seedsavers"><i class="pinterest-icon"></i></a>
				<a href="https://www.youtube.com/user/SSEHeritageFarm"><i class="youtube-play-icon"></i></a>
				<a href="https://www.instagram.com/seed_savers_exchange"><i class="instagram-icon"></i></a>
				<p>Subscribe to our newsletter for special offers and seed saving advice.</p>
				<!-- Begin MailChimp Signup Form -->
				<div id="mc_embed_signup">
					<form action="http://app.bronto.com/public/webform/render_form/k06r9yzi9pvjiqf7zsep4fcu6slan/e69e6d68559427cd31245d58afc3814d/addcontact " method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<div id="mc_embed_signup_scroll">
							<div class="mc-field-group">
								<input type="email" value="" name="EMAIL" class="required email input-mc-subscribe" id="mce-EMAIL" placeholder="Enter your email">
							</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>
							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true">
								<input type="text" name="b_4474a76b0fe1edee871ccdcee_530267ed05" tabindex="-1" value="">
							</div>
							<div class="clear">
								<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button-mc-subscribe">
							</div>
						</div>
					</form>
				</div>
				<!--End MailChimp Signup Form-->
			</div>
			<div class="col-md-3 col-md-offset-3">
				<h3>Who We Are</h3>
				<p>Seed Savers Exchange is a tax-exempt 501(c)3 nonprofit organization dedicated to the preservation of heirloom seeds.</p>
			</div>
			<div class="col-md-3">
				<h3>Contact Us</h3>
				<p>Seed Savers Exchange</br>
					3094 North Winn Road</br>
					Decorah, Iowa 52101</p>
				<h3><a href="/contact"><i class="envelope-icon"></i>Send Us a Message</a></h3>
				<h3 class="no-padding"><i class="phone-icon"></i>(563)382-5990</h3>
				<p class="no-padding">Monday to Friday, 9am - 5pm CST</p>
			</div>
		</div>
		<div class="row extra-top-padding">
			<div class="col-md-3 col-xs-6">
				<h3>Our Sites</h3>
				<p><a href="/">Home</a></p>
				<p><a href="http://exchange.seedsavers.org" target="_blank">Seed Exchange</a></p>
				<p><a href="http://blog.seedsavers.org" target="_blank">Blog</a></p>
			</div>
			<div class="col-md-3 col-xs-6">
				<h3>About Us</h3>
				<p><a href="/mission">Our Mission</a></p>
				<p><a href="/story">Our Story</a></p>
				<p><a href="/jobs">Job Opportunities</a></p>
				<p><a href="/visit">Visit Us</a></p>
			</div>
			<div class="col-md-3 col-xs-6">
				<h3>Customer Support</h3>
				<p><a href="/seed-policy">Safe Seed Pledge</a></p>
				<p><a href="/seed-policy">Satisfaction Guaranteed</a></p>
				<p><a href="/shipping-returns">Shipping Policy</a></p>
				<p><a href="/shipping-returns">Return Policy</a></p>
				<p><a href="/privacy">Privacy Policy</a></p>
				<p><a href="http://www.seedsavers.org/site/pdf/SSEOrganicCertificate.pdf" target="_blank">Organic Certificate</a></p>
				<!-- <p><a href="mailto:wholesale@seedsavers.org?subject=Wholesale%20Inquiry">Wholesale Inquiry</a></p> -->
				<!-- <p><a href="#">Print Order Form</a></p> -->
				<p><a href="/learn">Learn</a><p>
				<!-- <p><a href="#">FAQs</a></p> -->
			</div>
			<div class="col-md-3 col-xs-6">
				<h3>Quick Links</h3>
				<!-- <p><a href="#">What is an Heirloom</a></p> -->
				<!-- <p><a href="#">What is Organic</a></p> -->
				<p><a href="/department/vegetable-seeds">Heirloom Vegetable Seeds</a></p>
				<p><a href="/category/tomato">Heirloom Tomato Seeds</a></p>
				<p><a href="/category/bean">Heirloom Bean Seeds</a></p>
				<p><a href="/category/corn">Heirloom Corn Seeds</a></p>
				<p><a href="category/flowers">Heirloom Flower Seeds</a></p>
				<p><a href="/seed-rack">Seed Rack Retail Partners</a></p>
			</div>
		</div>
		<div class="row extra-top-padding">
			<p>{{translate '&copy; 2016 Seed Savers Exchange. Images on this site are protected by copyright - unauthorized use is not permitted.'}}</p>
		</div>
	</div>
</div>
