{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="item-details-image-gallery">
	{{#if showImages}}
		{{#if showImageSlider}}
			<ul class="bxslider" data-slider>
				{{#each images}}
					<li data-zoom>
						<img
							src="{{resizeImage url ../imageResizeId}}"
							alt="{{altimagetext}}"
							itemprop="image"
							data-loader="false">
					</li>
				{{/each}}
			</ul>
		{{else}}
			{{#with firstImage}}
				<div class="item-details-image-gallery-detailed-image " data-zoom>
					<img
						class="center-block"
						src="{{resizeImage url ../imageResizeId}}"
						alt="{{altimagetext}}"
						itemprop="image"
						data-loader="false">
				</div>
			{{/with}}

		{{/if}}
	{{/if}}
	<!--GO: Here are the heirloom and historic banners. They need a little tweaking from Pat - they look maybe a little strange when they're both visible? Also, Justus you should add unique classes to each image instead of keeping the styling in the html.-->
		{{#if heirloom_icon}}
		<div>
			<img class="heirloom-banner" src="http://sandbox.seedsavers.org//site/assets/heirloom-banner.png">
		</div>
		{{/if}}
		{{#if historic_icon}}
		<div>
			<img class="historic-banner" src="http://sandbox.seedsavers.org//site/assets/historic-banner.png">
		</div>
		{{/if}}
	<!--GO: End of banners.-->

	<div data-view="SocialSharing.Flyout.Hover"></div>
</div>
