{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div id="layout" class="myaccount-layout">
	<header id="site-header" class="myaccount-layout-header" data-view="Header"></header>

	<div id="main-container" class="myaccount-layout-container">

		<div class="myaccount-layout-breadcrumb" data-view="Global.Breadcrumb" data-type="breadcrumb"></div>
		<div class="myaccount-layout-error-placeholder"></div>

		<div class="row">
			<div class="col-sm-6">
				<h2 class="myaccount-layout-title">{{translate 'My Account'}}</h2>
			</div>

			<div class="col-sm-3">
				<a class="button-secondary button-full button-medium" href="http://www.seedsavers.org/department/retail-seed-rack">Retailer Store</a>
			</div>


			<div class="col-sm-3">
				<a class="button-secondary button-full button-medium" href="/quick-order">Quick Order</a>
			</div>


		</div>

		<!-- <h2 class="myaccount-layout-title">{{translate 'My Account'}}</h2> -->
		<div>
		<div class="myaccount-layout-row">
			<nav id="side-nav" class="myaccount-layout-side-nav" data-view="MenuTree"></nav>

			<div id="content" class="myaccount-layout-main"></div>
		</div>
	</div>
</div>

	<footer id="site-footer" class="myaccount-layout-footer" data-view="Footer"></footer>

</div>
