/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderHistory
define('OrderHistory.List.Tracking.Number.View'
,	[	'TrackingServices'
	,	'order_history_list_tracking_number.tpl'

	,	'Backbone'
	,	'underscore'
	]
,	function (
		TrackingServices
	,	order_history_list_tracking_number_tpl

	,	Backbone
	,	_
	)
{
	'use strict';

	//@class OrderHistory.List.Tracking.Number.View @extend Backbone.View
	return Backbone.View.extend({

		//@property {Function} template
		template: order_history_list_tracking_number_tpl

		//@property {Object} events
	,	events: {
			'click [data-action="tracking-number"]': 'trackingNumber'
		}
		//@method trackingNumber
	,	trackingNumber: function (e)
		{
			e.stopPropagation();
		}
		//@method getTrackingServiceUrl
	,	getTrackingServiceUrl: function (number)
		{
			return TrackingServices.getServiceUrl(number);
		}
		//@method getTrackingServiceName
	,	getTrackingServiceName: function (number)
		{
			return TrackingServices.getServiceName(number);
		}

		//@method getContext @return OrderHistory.List.Tracking.Number.View.Context
	,	getContext: function ()
		{
			var self = this
			,	tracking_numbers = this.model.get('trackingInformation');

			var serviceURL = '';
			var serviceName = '';
			var trackingNumber = '';
			var trackingNumbers = 0;

			if (tracking_numbers){
				serviceURL = tracking_numbers.URL;

				if (tracking_numbers.Number){
					for (var i = 0; i < tracking_numbers.Number.length; i++) {
				  	var number = tracking_numbers.Number[i];
						trackingNumber += ' ' + number;
						trackingNumbers++;
					}
				}
			}

			//@class OrderHistory.List.Tracking.Number.View.Context
			return {
				//@property {Boolean} isTrackingNumberCollectionEmptyddi
				isTrackingNumberCollectionEmpty: trackingNumbers == 0
				//@property {Boolean} isTrackingNumberCollectionLengthEqual1
			,	isTrackingNumberCollectionLengthEqual1: trackingNumbers === 1
				//@property {Boolean} showContentOnEmpty
			,	showContentOnEmpty: !!this.options.showContentOnEmpty
				//@property {String} contentClass
			,	contentClass: this.options.contentClass || ''
				//@property {String} firstTrackingNumberName
			,	firstTrackingNumberName: serviceName
				//@property {String} firstTrackingNumberURL
			,	firstTrackingNumberURL: serviceURL
				//@property {String} firstTrackingNumberText
			,	firstTrackingNumberText: trackingNumber
				//@property {Number} trackingNumbersLength
			,	trackingNumbersLength: trackingNumbers
				//@property {Boolean} collapseElements
			,	collapseElements: !!this.options.collapseElements
				//@property {Collection<Backboone.Model>} trackingNumbers
			,	trackingNumbers: tracking_numbers
				//@property {Boolean} showTrackPackagesLabel
			,	showTrackPackagesLabel: !_.isUndefined(this.options.showTrackPackagesLabel)? this.options.showTrackPackagesLabel : false
			};
		}
	});

});
