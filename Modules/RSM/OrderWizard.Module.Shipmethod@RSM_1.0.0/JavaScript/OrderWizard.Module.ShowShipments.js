/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderWizard.Module.Shipmethod
define(
	'OrderWizard.Module.ShowShipments'
,	[	'Wizard.Module'
	,	'Address.Details.View'
	,	'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'ItemViews.Cell.Navigable.View'
	,	'Profile.Model'

	,	'order_wizard_showshipments_module.tpl'

	,	'underscore'
	,	'jQuery'
	,	'Utils'
	]
,	function (
		WizardModule
	,	AddressDetailsView
	,	BackboneCompositeView
	,	BackboneCollectionView
	,	ItemViewsCellNavigableView
	,	ProfileModel

	,	order_wizard_showshipments_module_tpl

	,	_
	,	jQuery
	)
{
	'use strict';

	//@class OrderWizard.Module.ShowShipments @extends Wizard.Module
	return WizardModule.extend({

		//@property {Function} template
		template: order_wizard_showshipments_module_tpl

		//@property {Array} errors
	,	errors: ['ERR_NOT_SET_SHIPPING_METHODS', 'ERR_NOT_SET_SHIPPING_ADDRESS']

	,	shipMethodIsRequire: {
			errorMessage: _('Please select a delivery method').translate()
		,	errorCode: 'ERR_NOT_SET_SHIPPING_METHODS'
		}

	,	shipAddressIsRequire: {
			errorMessage: _('Please select a shipping address').translate()
		,	errorCode: 'ERR_NOT_SET_SHIPPING_ADDRESS'
		}

	,	deferredShipDateNotValid: {
			errorMessage: _('Please select a deferred ship greater than today').translate()
		,	errorCode: 'ERR_INVALID_DEFERRED_SHIP_DATE'
		}

		//@property {Object} events
	,	events: {
			'change [data-action="change-delivery-options"]': 'changeDeliveryOptions'
		,	'change [data-action="update-deferred-date"]': 'updateDeferredDate'
		}

		//@method render
	,	render: function ()
		{
			this.application = this.wizard.application;

			this.addressSource = this.options.useModelAddresses ? this.model.get('addresses') : this.wizard.options.profile.get('addresses');
			this.options.application = this.wizard.application;

			this._render();
		}

	,	initialize: function ()
		{
			WizardModule.prototype.initialize.apply(this, arguments);

			this.wizard.model.on('ismultishiptoUpdated', this.render, this);
			this.wizard.model.on('promocodeUpdated', this.render, this);

			BackboneCompositeView.add(this);
		}

		//@method isActive
		//@return {Boolean}
	,	isActive: function ()
		{
			return !this.model.get('ismultishipto');
		}

	,	isValid: function ()
		{
			if (this.model.shippingAddressIsRequired())
			{
				if (!this.model.get('shipmethod'))
				{
					return jQuery.Deferred().reject(this.shipMethodIsRequire);
				}
				else if (!this.model.get('shipaddress'))
				{
					return jQuery.Deferred().reject(this.shipAddressIsRequire);
				}
			}

			var defferedShipDate = this.model.get('deferredShipDate')
			if (defferedShipDate != null) {
				var dateDefferedShipDate = this.getDate(defferedShipDate);
				var currentDate = new Date();

				if (dateDefferedShipDate <= currentDate) {
					return jQuery.Deferred().reject(this.deferredShipDateNotValid);
				}
			}

			return jQuery.Deferred().resolve();
		}

		//takes in deferred date, parsed it out and return a date object
	,	getDate: function(defferedShipDate)
		{
			var arrayDeferredShipDate = defferedShipDate.split('/');
			var month = (parseFloat(arrayDeferredShipDate[0]) - 1);
			var day = parseFloat(arrayDeferredShipDate[1]);
			var year = parseFloat(arrayDeferredShipDate[2]);

			return new Date(year, month, day);
		}

	,	updateDeferredDate: function (e)
		{
			var value = this.$(e.target).val()
			,	self = this;

			if (value != this.model.get('deferredShipDate')) {
				this.model.set('deferredShipDate', value);
				this.step.disableNavButtons();
				this.model.save().always(function ()
				{
					self.render();
					self.step.enableNavButtons();
				});
			}
			this.$(e.target).datepicker('hide');
		}


		//@method changeDeliveryOptions
	,	changeDeliveryOptions: function (e)
		{
			var value = this.$(e.target).val()
			,	self = this;

			this.model.set('shipmethod', value);
			this.step.disableNavButtons();
			this.model.save().always(function ()
			{
				self.render();
				self.step.enableNavButtons();
			});
		}

		//@property {Object} childViews
	,	childViews: {
				'Shipping.Address': function ()
				{
					return new AddressDetailsView({
							hideActions: true
						,	hideDefaults: true
						,	manage: 'shipaddress'
					,	model: this.addressSource.get(this.model.get('shipaddress'))
					});
				}
		}

	,	checkSeedRackCustomer: function ()
		{
			var profile = ProfileModel.getInstance()

			var priceLevelInfo = profile.getPriceLevelInfo();

			if (priceLevelInfo.name == 'seedRack') {
				return true;
			}
			else {
				return false;
			}
		}

		//@method getContext
		//@returns {OrderWizard.Module.ShowShipments.Context}
	,	getContext: function ()
		{
			var self = this
			,	selected_shipmethod = this.model.get('shipmethods').findWhere({internalid: this.model.get('shipmethod')})
			,	shipping_methods = this.model.get('shipmethods').map(function (shipmethod)
				{
					return {
							name: shipmethod.get('name')
						,	rate_formatted: shipmethod.get('rate_formatted')
						,	internalid: shipmethod.get('internalid')
						,	isActive: shipmethod.get('internalid') === self.model.get('shipmethod')
					};
				});

			var psmShipMethods = this.model.get('psmShipMethods');
			var hasPsmShipMethods = (psmShipMethods != null && psmShipMethods.length > 0) ? true : false;
			var deferredShipDate = this.model.get('deferredShipDate');
			var displayDeferredShipDate = this.checkSeedRackCustomer();

			//@class OrderWizard.Module.ShowShipments.Context
			return {
					//@property {LiveOrder.Model} model
					model: this.model
					//@property {Boolean} showShippingInformation Indicate if the shipmethod select should be shown or not. Used when in SST all items are non shippable
				,	showShippingInformation: !!this.model.shippingAddressIsRequired()
					//@property {Boolean} showShippingAddress
				,	showShippingAddress: !!this.addressSource.get(this.model.get('shipaddress'))
					//@property {String} editUrl
				,	editUrl: this.options.edit_url
					//@property {Boolean} showEditButton
				,	showEditButton: !!this.options.edit_url
					//@property {Boolean}
				,	showSelectedShipmethod: !!selected_shipmethod
					//@property {Object} selectedShipmethod
				,	selectedShipmethod: selected_shipmethod
					//@property {Array} shippingMethods
				,	shippingMethods: shipping_methods
					//@property {Boolean} showShippingMetod
				,	showShippingMetod: !this.options.hideShippingMethod
				,	hasPsmShipMethods: hasPsmShipMethods
				,	psmShipMethods: psmShipMethods
				,	deferredShipDate: deferredShipDate
				,	displayDeferredShipDate: displayDeferredShipDate
			};
		}
	});
});
