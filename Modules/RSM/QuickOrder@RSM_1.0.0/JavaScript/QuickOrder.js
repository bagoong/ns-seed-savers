/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrder
define('QuickOrder'
,	[
		'QuickOrder.Router'

	,	'underscore'
	,	'Utils'
	]
,	function (
		QuickOrderRouter

	,	_
	)
{
	'use strict';

	//@class QuickOrder @extend ApplicationModule
	return	{
		//@property {MenuItem} MenuItem
		// MenuItems: {
		// 	parent: 'orders'
		// ,	id: 'quickorder'
		// ,	name: _('Quick Order').translate()
		// ,	url: 'quick-order'
		// ,	index: 5
		// ,	permission: 'transactions.tranFind.1,transactions.tranEstimate.1'
		// }

		//@method mountToApp
		//@param {ApplicationSkeleton} application
		//@return {QuickOrderRouter} Returns an instance of the QuickOrder router used by the current module
		mountToApp: function (application)
		{
			return new QuickOrderRouter(application);
		}
	};
});
