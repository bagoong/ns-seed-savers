/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrderWizard
define(
	'QuickOrderWizard.View'
,	[
		'Wizard.View'
	,	'Wizard.StepNavigation.View'

	,	'quickorder_wizard_layout.tpl'

	,	'underscore'
	,	'Utils'
	]
,	function (
		WizardView
	,	WizardStepNavigationView

	,	quickorder_wizard_layout_tpl

	,	_
	)
{
	'use strict';

	// @class QuickOrderWizard.View @extends Wizard.View
	return WizardView.extend({

		//@property {Function} template
		template: quickorder_wizard_layout_tpl

		//@property {String} page_header
	,	page_header: _('Quick Order').translate()

		//@property {String} bodyClass This property indicate the class used on the body to remove the My Account side menu
	,	bodyClass: 'force-hide-side-nav'

		// @method getBreadcrumbPages
		// @return {BreadcrumbPage}
	,	getBreadcrumbPages: function ()
		{
			return {href: '/quick-order', text: _('Quick Order').translate()};
		}

		//@property {ChildViews} childViews
	,	childViews: {
	    	'Wizard.StepNavigation': function()
	    	{
	    		return new WizardStepNavigationView({wizard: this.wizard});
	    	}
	    }

		// @method getContext
		// @return {QuickOrderWizard.View.Context}
	,	getContext: function ()
		{
			// @class QuickOrderWizard.View.Context
			return {
				// @property {String} pageHeader
				pageHeader: this.page_header
			};
			// @class QuickOrderWizard.View
		}
   });
});
