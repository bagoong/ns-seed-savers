/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrderWizard
define('QuickOrderWizard.Module.Message'
,	[
		'SC.Configuration'
	,	'Wizard.Module'

	,	'quickorder_wizard_module_message.tpl'

	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration
	,	WizardModule

	,	quickorder_wizard_module_message_tpl

	,	_
	)
{
	'use strict';

	//@class WizardModule.Module.Message @extend Wizard.Message
	return WizardModule.extend({

		// @property {Function} template
		template: quickorder_wizard_module_message_tpl

		//@method getContext
		//@return {QuickOrderWizard.Module.Message.Context}
	,	getContext: function ()
		{
			//@class QuickOrderWizard.Module.Message.Context
			return {
				// @property {String} pageHeader
				pageHeader: _('Request a Quote').translate()
				// @property {String} message
			,	message: this.options.message || this.wizard.getCurrentStep().bottomMessage
			};
			//@class QuickOrderWizard.Module.Message
		}
	});
});
