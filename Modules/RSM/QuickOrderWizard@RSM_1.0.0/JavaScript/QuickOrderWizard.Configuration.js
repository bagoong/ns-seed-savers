/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrderWizard
define('QuickOrderWizard.Configuration'
,	[
		'OrderWizard.Module.CartItems'
	,	'OrderWizard.Module.ShowShipments'

	,	'QuickOrderWizard.Module.Header'
	,	'QuickOrderWizard.Module.Message'
	,	'QuickOrderWizard.Module.QuickAdd'
	,	'QuickOrderWizard.Module.Comments'
	,	'QuickOrderWizard.Module.Items'
	,	'OrderWizard.Module.Address.Shipping'
	,	'OrderWizard.Module.Title'
	,	'QuickOrderWizard.Module.Confirmation'

	,	'underscore'
	,	'Utils'

	]
,	function (
		OrderWizardModuleCartItems
	,	OrderWizardModuleShowShipments

	,	QuickOrderWizardModuleHeader
	,	QuickOrderWizardModuleMessage
	,	QuickOrderWizardModuleQuickAdd
	,	QuickOrderWizardModuleComments
	,	QuickOrderWizardModuleItems
	,	OrderWizardModuleAddressShipping
	,	OrderWizardModuleTitle
	,	QuickOrderWizardModuleConfirmation

	,	_
	)
{
	'use strict';

	//@class QuickOrderWizard.Configuration Defines the configuration for the Request Quote Wizard module
	return {
		steps: [
			{
				name: ''
			,	steps: [
					{
						url: 'quick-order'
					,	name: _('Quick Order').translate()
					,	hideBackButton: true
					,	hideContinueButton: false
					//,	bottomMessage: _('Once your quote has been submitted, a sales representative will contact you in <strong>XX business days</strong>. For immediate assistance call us at <strong>(000)-XXX-XXXX</strong> or email us at <a href="mailto:xxxx@xxxx.com">xxxx@xxxx.com</a>').translate()
					,	continueButtonLabel: _('Add To Cart').translate()
					,	modules: [
							QuickOrderWizardModuleHeader
						,	QuickOrderWizardModuleMessage
						,	[	OrderWizardModuleTitle
							,	{
									title: _('Add Items').translate()
								}
							]
						,	QuickOrderWizardModuleQuickAdd
						,	QuickOrderWizardModuleItems
						// ,	[	OrderWizardModuleTitle
						// 	,	{
						// 			title: _('Choose a Shipping Address').translate()
						// 		}
						// 	]
						// ,	OrderWizardModuleAddressShipping
						// ,	[
						// 		OrderWizardModuleTitle
						// 	,	{
						// 			title: _('Comments').translate()
						// 		}
						// 	]
						// ,	QuickOrderWizardModuleComments
						]
					,	save: function ()
						{
							_.first(this.moduleInstances).trigger('change_label_continue', _('Processing...').translate());

							var self = this
							,	submit_opreation = this.wizard.model.submit();

							submit_opreation.always(function ()
							{
								_.first(self.moduleInstances).trigger('change_label_continue', _('Submit Quote Request').translate());
							});

							return submit_opreation;
						}
					}
				,	{
						url: 'quick-order-confirmation'
					,	name: _('Quick Order').translate()
					,	hideBackButton: true
					,	hideContinueButton: true
					//,	confirmationMessage: _('A sales representative will contact you in <strong>XX business days</strong>.').translate()
					,	modules: [
							QuickOrderWizardModuleConfirmation
						,	[
							// 	OrderWizardModuleCartItems
							// ,	{
							// 		hide_edit_cart_button: true
							// 	,	showOpenedAccordion: true
							// 	}
							]
						// ,	[	OrderWizardModuleShowShipments
						// 	,	{
						// 			hideShippingMethod: true
						// 		}
						// 	]
						// ,	[	QuickOrderWizardModuleComments
						// 	,	{
						// 			is_read_only: true
						// 		,	title: _('Comments').translate()
						// 		,	hide_title: false
						// 		}
						// 	]
						]
					}
				]
			}
		]
	};
});
