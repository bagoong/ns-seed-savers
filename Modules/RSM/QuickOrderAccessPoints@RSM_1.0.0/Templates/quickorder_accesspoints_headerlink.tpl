{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<a class="{{#if hasClass}} {{className}} {{else}}quickorder-accesspoints-headerlink-link{{/if}}" href="#" data-hashtag="#quick-order" data-touchpoint="customercenter" title="{{translate 'Request a Quote'}}">
	{{translate 'Quick Order'}}
</a>
